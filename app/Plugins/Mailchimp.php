<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 26.03.2019
 * Time: 7:41
 */

namespace App\Plugins;

class Mailchimp
{
    protected $server_url;
    protected $api_key;
    protected $list_id;

    public function __construct($api_key, $list_id)
    {
        $this->api_key = $api_key;
        $this->list_id = $list_id;

        $dc = substr($api_key, strpos($api_key, '-') + 1);
        $this->server_url = 'https://' . $dc . '.api.mailchimp.com/3.0';
    }

    /**
     * Push user to mailchimp list
     * return mailchimp user_id
     */
    public function pushUser($email)
    {
        $action_url = '/lists/' . $this->list_id . '/members';
        $data = ["email_address" => $email, "status" => "subscribed"];

        $curl = curl_init();

        $data_string = json_encode($data);
        curl_setopt($curl, CURLOPT_URL, $this->server_url . $action_url);
        curl_setopt($curl, CURLOPT_USERPWD, "anystring:$this->api_key");
        curl_setopt($curl, CURLOPT_HTTPHEADER, [
            "content-type: application/json"
        ]);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = json_decode(curl_exec($curl), true);

        return $response['id'];
    }

}