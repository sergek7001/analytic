<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plugins\Mailchimp;

class MailchimpController extends Controller
{
    public $res = [];

    public function __construct()
    {
        $this->res += [
            'controller_name' => 'mailchimp',
        ];
    }

    public function index()
    {
        $this->res += [
            'action_name' => 'index',
        ];

        return view('admin/mailchimp/index', $this->res);
    }

    public function pushToMailchimp(Request $request)
    {
        if ($request->except('api_key') && $request->except('list_id') && $request->except('email')) {
            $mailchimp = new Mailchimp($request->input('api_key'), $request->input('list_id'));
            return json_encode($mailchimp->pushUser($request->input('email')));
        }
    }
}
