<?php

namespace App\Http\Controllers\Visit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Site;
use App\Visit;
use App\Visitor;

class VisitorController extends Controller
{
    public $res = [];

    public function __construct()
    {
        $this->res += [
            'controller_name' => 'visitor',
        ];
    }

    public function index()
    {
        $visitors = Visitor::all();

        $this->res += [
            'action_name' => 'index',
            'visitors' => $visitors,
        ];

        return view('admin/visitor/index', $this->res);
    }
    
}
