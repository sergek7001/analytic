<?php

namespace App\Http\Controllers\Visit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Site;
use App\Visit;
use App\Visitor;

class StoreController extends Controller
{
    public function __construct()
    {
    }

    public function store(Request $request)
    {
        $site = null;
        if(Site::where('url', $request->input('host'))->count()){
            $site = Site::where('url', $request->input('host'))->first();
        }else{
            $site = new Site;
            $site->url = $request->input('host');
            $site->save();
        }

        //$visitor = Visitor::firstOrCreate(['ip' => $request->ip(), 'user_agent' => $request->input('user_agent')]);
        $visitor = null;
        if(Visitor::where('ip', $request->ip())->where('user_agent', $request->input('user_agent'))->count()){
            $visitor = Visitor::where('ip', $request->ip())->where('user_agent', $request->input('user_agent'))->first();
        }else{
            $visitor = new Visitor;
            $visitor->ip = $request->ip();
            $visitor->user_agent = $request->input('user_agent');
            $visitor->save();
        }

        $visit = new Visit;
        $visit->site_id = $site->id;
        $visit->visitor_id = $visitor->id;
        $visit->route = $request->input('url');
        $visit->page_open = $request->input('start');
        $visit->page_close = $request->input('end');
        $visit->save();

    }
}
