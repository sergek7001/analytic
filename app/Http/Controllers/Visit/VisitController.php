<?php

namespace App\Http\Controllers\Visit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Site;
use App\Visit;
use App\Visitor;

class VisitController extends Controller
{
    public $res = [];

    public function __construct()
    {
        $this->res += [
            'controller_name' => 'visit',
        ];
    }

    public function index()
    {
        $visits = Visit::all();
        $sites = Site::alL();
        $visitors = Visitor::alL();

        $this->res += [
            'action_name' => 'index',
            'visits' => $visits,
            'visitors' => $visitors,
            'sites' => $sites,
        ];

        return view('admin/visit/index', $this->res);
    }
}
