<?php

namespace App\Http\Controllers\Visit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Site;
use App\Visit;
use App\Visitor;

class DashboardController extends Controller
{
    public $res = [];

    public function __construct()
    {
        $this->res += [
            'controller_name' => 'dashboard',
        ];
    }

    public function index()
    {
        $this->res += [
            'action_name' => 'index',
        ];

        return view('admin/dashboard/index', $this->res);
    }
}
