<?php

namespace App\Http\Controllers\Visit;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Site;
use App\Visit;
use App\Visitor;

class SiteController extends Controller
{
    public $res = [];
    
    public function __construct()
    {
        $this->res += [
            'controller_name' => 'site',
        ];
    }

    public function index()
    {
        $sites = Site::all();

        $this->res += [
            'action_name' => 'index',
            'sites' => $sites,
        ];

        return view('admin/site/index', $this->res);
    }


}
