<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Email;

class EmailsController extends Controller
{

    public $controller_name = 'emails';
    public $action_name = '';
    public $user;
    public $user_id = 0;
    public $user_name = '';

    private function set_user_param()
    {
        $this->user = Auth::user();
        $this->user_id = $this->user->id;
        $this->user_name = $this->user->name;
    }

    public function getResult()
    {
        $result = [
            'controller_name' => $this->controller_name,
            'action_name' => $this->action_name,
            'user_id' => $this->user_id,
            'user_name' => $this->user_name,
        ];

        return $result;
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->action_name = 'index';
        $this->set_user_param();

        $result = $this->getResult();

        $emails = Email::all()->sortBy('position');

        $result += [
            'emails' => $emails,
        ];

        return view('admin.emails.index', $result);
    }

    public function edit(Request $request, $id)
    {
        $this->action_name = 'edit';
        $this->set_user_param();

        $result = $this->getResult();

        $status = $request->session()->get('status');;

        $email = Email::where('id', $id)->first();

        $result += [
            'email' => $email,
        ];

        return view('admin.emails.edit', $result);
    }


    public function update(Request $request)
    {
        $this->set_user_param();
        $email = Email::where('id', $request->input("email_id"))->first();

        $email->email = $request->input("email");

        $email->save();

        return json_encode('success');
    }


    public function create()
    {
        $this->action_name = 'create';
        $this->set_user_param();

        $result = $this->getResult();

        return view('admin.emails.create', $result);
    }


    public function add(Request $request)
    {
        $this->rePosition();
        $data = new Email();
        $data->email = $request->input("email");
        $max_position = Email::max('position');
        $data->position = $max_position + 1;
        $data->save();
        return redirect('private/emails');
    }


    public function delete($id)
    {
        Email::where('id', $id)->delete();
        $this->rePosition();
        return json_encode('success');
    }


    public function up($id)
    {
        $this->action_name = 'up';
        $this->set_user_param();

        $this->rePosition();

        $email1 = Email::where('id', $id)->first();
        $position = $email1->position;
        $email2 = Email::where('position', $position - 1)->first();
        $email1->position = $position - 1;
        $email2->position = $position;
        $email1->save();
        $email2->save();

        return redirect('/private/emails');
    }


    public function down($id)
    {
        $this->action_name = 'down';
        $this->set_user_param();

        $this->rePosition();

        $email1 = Email::where('id', $id)->first();
        $position = $email1->position;
        $email2 = Email::where('position', $position + 1)->first();
        $email1->position = $position + 1;
        $email2->position = $position;
        $email1->save();
        $email2->save();

        return redirect('/private/emails');
    }


    public function rePosition()
    {
        $emails = Email::all()->sortBy('position');
        $i = 0;
        foreach ($emails as $email) {
            $i++;
            $email->position = $i;
            $email->save();
        }
    }


}
