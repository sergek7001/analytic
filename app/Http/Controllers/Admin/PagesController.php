<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Page;
use App\PageLanguage;
use App\Language;

class PagesController extends Controller
{
    public $controller_name = 'pages';
    public $action_name = '';
    public $user;
    public $user_id = 0;
    public $user_name = '';

    private function set_user_param()
    {
        $this->user = Auth::user();
        $this->user_id = $this->user->id;
        $this->user_name = $this->user->name;
    }

    public function getResult()
    {
        $result = [
            'controller_name' => $this->controller_name,
            'action_name' => $this->action_name,
            'user_id' => $this->user_id,
            'user_name' => $this->user_name,
        ];

        return $result;
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->action_name = 'index';
        $this->set_user_param();

        $result = $this->getResult();

        $pages = Page::all()->sortBy('position');

        $result += [
            'pages' => $pages,
        ];

        return view('admin.pages.index', $result);
    }

    public function edit(Request $request, $id)
    {
        $this->action_name = 'edit';
        $this->set_user_param();

        $result = $this->getResult();

        $status = $request->session()->get('status');;

        $languages = Language::all()->sortBy('position');

        foreach ($languages as $language) {
            $page_language = PageLanguage::where('page_id', $id)->where('language_id', $language->id)->first();
            if ($page_language == null) {
                $page_language = new PageLanguage([
                    'page_id' => $id,
                    'language_id' => $language->id,
                ]);
                $page_language->save();
            }
        }

        $page = Page::where('id', $id)->with('page_languages')->first();

        $result += [
            'languages' => $languages,
            'page' => $page,
        ];

        return view('admin.pages.edit', $result);
    }


    public function update(Request $request)
    {
        $this->action_name = 'update';
        $this->set_user_param();

        $result = $this->getResult();

        $page_language = PageLanguage::where('page_id', $request->input("page_id"))->where('language_id', $request->input("language_id"))->first();

        $page_language->menu = $request->input("menu");
        $page_language->tag_title = $request->input("tag_title");
        $page_language->meta_description = $request->input("meta_description");
        $page_language->meta_keywords = $request->input("meta_keywords");
        $page_language->title = $request->input("title");
        $page_language->text = $request->input("text");
        $page_language->footer_text = $request->input("footer_text");
        $page_language->on_image_title = $request->input("on_image_title");
        $page_language->on_image_alt = $request->input("on_image_alt");
        $page_language->save();

        return json_encode('success');
    }

}
