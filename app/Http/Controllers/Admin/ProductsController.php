<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Page;
use App\PageLanguage;
use App\Language;
use App\Product;
use App\ProductImage;
use App\ProductLanguage;
use App\ProductImageLanguage;
use App\ProductSize;
use App\Plagins\Translit;
use Illuminate\Support\Str;

class ProductsController extends Controller
{
    public $controller_name = 'products';
    public $action_name = '';
    public $user;
    public $user_id = 0;
    public $user_name = '';

    private function set_user_param()
    {
        $this->user = Auth::user();
        $this->user_id = $this->user->id;
        $this->user_name = $this->user->name;
    }

    public function getResult()
    {
        $result = [
            'controller_name' => $this->controller_name,
            'action_name' => $this->action_name,
            'user_id' => $this->user_id,
            'user_name' => $this->user_name,
        ];

        return $result;
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->action_name = 'index';
        $this->set_user_param();

        $result = $this->getResult();

        $products = Product::all()->sortBy('position');

        $result += [
            'products' => $products,
        ];

        return view('admin.products.index', $result);
    }

    public function create()
    {
        $this->action_name = 'create';
        $this->set_user_param();

        $result = $this->getResult();

        $languages = Language::all()->sortBy('position');

        $code = rand(100, 999) . '-' . rand(100, 999);
        while (Product::where('code', $code)->count() != 0) {
            $code = rand(100, 999) . '-' . rand(100, 999);
        }

        $result += [
            'languages' => $languages,
            'code' => $code,
        ];

        return view('admin.products.create', $result);
    }

    public function edit(Request $request, $id)
    {
        $this->action_name = 'edit';
        $this->set_user_param();

        $result = $this->getResult();

        //$status = $request->session()->get('status');           //что это?
        $languages = Language::all()->sortBy('position');

        $product = Product::where('id', $id)->first();
        $product_languages = ProductLanguage::where('product_id', $id)->get();
        $category = Category::where('id', $product->category_id)->first();

        $result += [
            'languages' => $languages,
            'category' => $category,
            'product' => $product,
            'product_languages' => $product_languages,
        ];

        return view('admin.products.edit', $result);
    }

    public function updateOrCreate(Request $request)
    {
        $product = new Product;
        if ($request->input('product_id')) {
            $product = Product::where('id', $request->input('product_id'))->first();
        }

        $product->category_id = $request->input('category');
        $product->name = $request->input('name');
        $product->route = Str::slug(Translit::translit($request->input('name')."_".$request->input('code')), '-');
        $product->code = $request->input('code');
        $product->save();

        $languages = Language::all()->sortBy('position');

        foreach ($languages as $language) {
            $product_language = ProductLanguage::where('product_id', $product->id)->where('language_id', $language->id)->first();
            if ($product_language == null) {
                $product_language = new ProductLanguage([
                    'product_id' => $product->id,
                    'language_id' => $language->id,
                ]);

                if ($language->id == $request->input('language_id')) {
                    $product_language->title = $request->input('menu');
                    $product_language->text = $request->input('text');
                    $product_language->tag_title = $request->input('tag_title');
                    $product_language->meta_description = $request->input('meta_description');
                    $product_language->meta_keywords = $request->input('meta_keywords');
                }
                $product_language->save();
            }
        }

        return json_encode($product->id);
    }
}
