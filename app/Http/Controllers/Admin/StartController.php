<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StartController extends Controller
{
    public $admins_info;

    public $controller_name = 'start';
    public $action_name = '';
    public $user;
    public $user_id = 0;
    public $user_name = '';

    public function __construct(Request $request){
        $this->middleware('breadcrumbs');
        //$this->f=new AdminFactory();
        $this->general=AdminGeneral::getInstance();
        $this->general->init($request);
        $this->request=$request;

    }

    private function set_user_param()
    {
        $this->user = Auth::user();
        $this->user_id = $this->user->id;
        $this->user_name = $this->user->name;
    }

    public function getResult()
    {
        $result = [
            'controller_name' => $this->controller_name,
            'action_name' => $this->action_name,
            'user_id' => $this->user_id,
            'user_name' => $this->user_name,
        ];

        return $result;
    }

    public function showStart(){



        $this->action_name = 'index';
        $this->set_user_param();

        $result = $this->getResult();

        /*  if(Auth::guard('admin')->check())
         {dump("admin") ;}
         elseif(Auth::guard('web')->check())
         {dump("user") ;} */
        $this->menu=$this->makeMenu();
        // $this->start_info=$this->f->getClients($this->general->admin_type,$this->request)->showStart()->init($this->request);
        $this->admin=Auth::user();

        $this->admins=User::all();
        //$this->status_info=$this->general->arrays->status;
        $result += [
            'menu'=>$this->menu,
            'admin'=>$this->admin,
            'admin_type'=>$this->general->admin_type,
            'breadcrumbs'=>$this->request->get('breadcrumbs'),
            'benutzer'=>$this->admins,
            //'status'=>$this->general->arrays->status,
            //'status_info'=>$this->status_info,
        ];
        return view('admin.start', $result);
    }
}
