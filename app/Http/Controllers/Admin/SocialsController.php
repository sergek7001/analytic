<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Social;

class SocialsController extends Controller
{
    public $controller_name = 'socials';
    public $action_name = '';
    public $user;
    public $user_id = 0;
    public $user_name = '';

    private function set_user_param()
    {
        $this->user = Auth::user();
        $this->user_id = $this->user->id;
        $this->user_name = $this->user->name;
    }

    public function getResult()
    {
        $result = [
            'controller_name' => $this->controller_name,
            'action_name' => $this->action_name,
            'user_id' => $this->user_id,
            'user_name' => $this->user_name,
        ];

        return $result;
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->action_name = 'index';
        $this->set_user_param();

        $result = $this->getResult();

        $socials = Social::all();

        $result += [
            'socials' => $socials,
        ];

        return view('admin.socials.index', $result);
    }

    public function changeActive(Request $request, $id)
    {


        $socials = Social::where('id', $id)->first();
        $socials->is_active = $request->input('is_active');
        $socials->save();



        return json_encode('success');
    }

    public function changeUrl(Request $request, $id)
    {
        $socials = Social::where('id', $id)->first();
        $socials->route = $request->input('url');
        $socials->save();
        return json_encode('success');
    }
}
