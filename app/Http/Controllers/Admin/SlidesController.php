<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Slide;
use App\SlideLanguage;
use App\Language;

class SlidesController extends Controller
{

    public $controller_name = 'slides';
    public $action_name = '';
    public $user;
    public $user_id = 0;
    public $user_name = '';

    private function set_user_param()
    {
        $this->user = Auth::user();
        $this->user_id = $this->user->id;
        $this->user_name = $this->user->name;
    }

    public function getResult()
    {
        $result = [
            'controller_name' => $this->controller_name,
            'action_name' => $this->action_name,
            'user_id' => $this->user_id,
            'user_name' => $this->user_name,
        ];

        return $result;
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->action_name = 'index';
        $this->set_user_param();

        $result = $this->getResult();

        $slides = Slide::all()->sortBy('position');

        $result += [
            'slides' => $slides,
        ];

        return view('admin.slides.index', $result);
    }

    public function edit(Request $request, $id)
    {
        $this->action_name = 'edit';
        $this->set_user_param();

        $result = $this->getResult();

        $status = $request->session()->get('status');;

        $languages = Language::all()->sortBy('position');

        foreach ($languages as $language) {
            $slide_language = SlideLanguage::where('slide_id', $id)->where('language_id', $language->id)->first();
            if ($slide_language == null) {
                $slide_language = new SlideLanguage([
                    'slide_id' => $id,
                    'language_id' => $language->id,
                ]);
                $slide_language->save();
            }
        }

        $slide = Slide::where('id', $id)->with('slide_languages')->first();

        $result += [
            'languages' => $languages,
            'slide' => $slide,
        ];

        return view('admin.slides.edit', $result);
    }


    public function update(Request $request)
    {
        $this->action_name = 'update';
        $this->set_user_param();

        $result = $this->getResult();

        $slide_language = SlideLanguage::where('slide_id', $request->input("slide_id"))->where('language_id', $request->input("language_id"))->first();

        $slide_language->title = $request->input("title");
        $slide_language->text = $request->input("text");
        $slide_language->button = $request->input("button");
        $slide_language->link = $request->input("link");

        $slide_language->save();

        return json_encode('success');
    }


    public function create()
    {
        $this->action_name = 'create';
        $this->set_user_param();

        $result = $this->getResult();

        return view('admin.slides.create', $result);
    }


    public function add(Request $request)
    {

        $this->rePosition();
        $data = new Slide();
        $data->name = $request->input("name");
        $max_position=Slide::max('position');
        $data->position = $max_position+1;
        $data->save();
        return redirect('private/slides');
    }


    public function delete($id)
    {
        Slide::where('id', $id)->delete();
        $this->rePosition();
        return json_encode('success');
    }


    public function up($id)
    {
        $this->action_name = 'up';
        $this->set_user_param();

        $this->rePosition();

        $slide1 = Slide::where('id', $id)->first();
        $position=$slide1->position;
        $slide2 = Slide::where('position', $position-1)->first();
        $slide1->position=$position-1;
        $slide2->position=$position;
        $slide1->save();
        $slide2->save();

        return redirect('/private/slides');
    }


    public function down($id)
    {
        $this->action_name = 'down';
        $this->set_user_param();

        $this->rePosition();

        $slide1 = Slide::where('id', $id)->first();
        $position=$slide1->position;
        $slide2 = Slide::where('position', $position+1)->first();
        $slide1->position=$position+1;
        $slide2->position=$position;
        $slide1->save();
        $slide2->save();

        return redirect('/private/slides');
    }


    public function rePosition()
    {
        $slides = Slide::all()->sortBy('position');
        $i=0;
        foreach ($slides as $slide) {
            $i++;
            $slide->position=$i;
            $slide->save();
        }
    }


}
