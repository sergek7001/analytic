<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Phone;

class PhonesController extends Controller
{

    public $controller_name = 'phones';
    public $action_name = '';
    public $user;
    public $user_id = 0;
    public $user_name = '';

    private function set_user_param()
    {
        $this->user = Auth::user();
        $this->user_id = $this->user->id;
        $this->user_name = $this->user->name;
    }

    public function getResult()
    {
        $result = [
            'controller_name' => $this->controller_name,
            'action_name' => $this->action_name,
            'user_id' => $this->user_id,
            'user_name' => $this->user_name,
        ];

        return $result;
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->action_name = 'index';
        $this->set_user_param();

        $result = $this->getResult();

        $phones = Phone::all()->sortBy('position');

        $result += [
            'phones' => $phones,
        ];

        return view('admin.phones.index', $result);
    }

    public function edit(Request $request, $id)
    {
        $this->action_name = 'edit';
        $this->set_user_param();

        $result = $this->getResult();

        $status = $request->session()->get('status');;

        $phone = Phone::where('id', $id)->first();

        $result += [
            'phone' => $phone,
        ];

        return view('admin.phones.edit', $result);
    }


    public function update(Request $request)
    {
        $this->set_user_param();
        $phone = Phone::where('id', $request->input("phone_id"))->first();

        $phone->phone = $request->input("phone");

        $phone->save();

        return json_encode('success');
    }


    public function create()
    {
        $this->action_name = 'create';
        $this->set_user_param();

        $result = $this->getResult();

        return view('admin.phones.create', $result);
    }


    public function add(Request $request)
    {

        $this->rePosition();
        $data = new Phone();
        $data->phone = $request->input("phone");
        $max_position = Phone::max('position');
        $data->position = $max_position + 1;
        $data->save();
        return redirect('private/phones');
    }


    public function delete($id)
    {
        Phone::where('id', $id)->delete();
        $this->rePosition();
        return json_encode('success');
    }


    public function up($id)
    {
        $this->action_name = 'up';
        $this->set_user_param();

        $this->rePosition();

        $phone1 = Phone::where('id', $id)->first();
        $position = $phone1->position;
        $phone2 = Phone::where('position', $position - 1)->first();
        $phone1->position = $position - 1;
        $phone2->position = $position;
        $phone1->save();
        $phone2->save();

        return redirect('/private/phones');
    }


    public function down($id)
    {
        $this->action_name = 'down';
        $this->set_user_param();

        $this->rePosition();

        $phone1 = Phone::where('id', $id)->first();
        $position = $phone1->position;
        $phone2 = Phone::where('position', $position + 1)->first();
        $phone1->position = $position + 1;
        $phone2->position = $position;
        $phone1->save();
        $phone2->save();

        return redirect('/private/phones');
    }


    public function rePosition()
    {
        $phones = Phone::all()->sortBy('position');
        $i = 0;
        foreach ($phones as $phone) {
            $i++;
            $phone->position = $i;
            $phone->save();
        }
    }


}
