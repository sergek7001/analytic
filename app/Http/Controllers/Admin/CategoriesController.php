<?php

namespace App\Http\Controllers\Admin;

//use Illuminate\Http\Request;
use Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Category;
use App\CategoryLanguage;
use App\Language;

class CategoriesController extends Controller
{
    public $controller_name = 'categories';
    public $action_name = '';
    public $user;
    public $user_id = 0;
    public $user_name = '';

    private function set_user_param()
    {
        $this->user = Auth::user();
        $this->user_id = $this->user->id;
        $this->user_name = $this->user->name;
    }

    public function getResult()
    {
        $result = [
            'controller_name' => $this->controller_name,
            'action_name' => $this->action_name,
            'user_id' => $this->user_id,
            'user_name' => $this->user_name,
        ];

        return $result;
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->action_name = 'index';
        $this->set_user_param();

        $result = $this->getResult();

        return view('admin.categories.index', $result);
    }

    public function edit(Request $request, $id)
    {
        $this->action_name = 'edit';
        $this->set_user_param();

        $request = Request::all();

        $result = $this->getResult();

        //$status = $request->session()->get('status');;

        $languages = Language::all()->sortBy('position');

        foreach ($languages as $language) {
            $category_language = CategoryLanguage::where('category_id', $id)->where('language_id', $language->id)->first();
            if ($category_language == null) {
                $category_language = new CategoryLanguage([
                    'category_id' => $id,
                    'language_id' => $language->id,
                ]);
                $category_language->save();
            }
        }

        $category = Category::where('id', $id)->with('category_languages')->first();

        $result += [
            'languages' => $languages,
            'category' => $category,
        ];

        return view('admin.categories.edit', $result);
    }

    public function edit_image(Request $request)
    {
        $request = Request::all();

        foreach ($request['file'] as $key => $value) {

            //$input = $request->all();

            $name = $request['id'] . '-image' . '.jpeg';
            $value->move(public_path('/images/site/categories/'), $name);

            $targ_w = 1264;
            $targ_h = 800;
            $jpeg_quality = 90;

            $src = public_path('/images/site/categories/' . $name);
            $img_r = imagecreatefromjpeg($src);
            $dst_r = ImageCreateTrueColor($targ_w, $targ_h);

            imagecopyresampled($dst_r, $img_r, 0, 0, $request['x1'], $request['y1'],
                $targ_w, $targ_h, $request['w'], $request['h']);

            //header('Content-type: image/jpeg');
            imagejpeg($dst_r, public_path('/images/site/categories/' . $name), $jpeg_quality);

            $category = Category::where('id', $request['id'])->first();
            $category->image = $name;
            $category->save();

        }

        return redirect('/admin/categories/edit/' . $request['id']);
    }

    public function del_image(){
        $request = Request::all();
        $category = Category::where('id', $request['id'])->first();
        unlink(public_path('/images/site/categories/').$category->image);
        $category->image = '';
        $category->save();

        return redirect('/admin/categories/edit/' . $request['id']);
    }


    public function update(Request $request)
    {
        $this->action_name = 'update';
        $this->set_user_param();

        $result = $this->getResult();
        $request = Request::all();

        $category_id = $request["category_id"];
        $language_id = $request["language_id"];
        $menu = $request["menu"];
        if ($language_id == 1) {
            $category = Category::find($category_id);
            //$category->route_name=$menu
        }


        $category_language = CategoryLanguage::where('category_id', $category_id)->where('language_id', $language_id)->first();

        $category_language->menu = $request["menu"];
        $category_language->tag_title = $request["tag_title"];
        $category_language->meta_description = $request["meta_description"];

        $category_language->save();

        $category = Category::where('id', $category_id)->first();
        $category->name = $request["name"];
        $category->save();

        return json_encode('success');
    }


    public function create()
    {
        $this->action_name = 'create';
        $this->set_user_param();

        $result = $this->getResult();

        return view('admin.categories.create', $result);
    }


    public function addCategory(Request $request)
    {
        $request = Request::all();

        $parent_id = $request["parent_id"];
        $data = new Category();
        $data->name = $request["name"];
        $data->route = str_slug($st = preg_replace("/[^a-zA-ZА-Яа-я0-9\s]/", " ", $data->name), '-');
        $data->parent_id = $parent_id;
        $max_position = Category::where('parent_id', $parent_id)->max('position');
        //dd($max_position);
        $data->position = $max_position + 1;
        $data->save();
        //$this->rePosition();
        return json_encode('success');
    }


    public function delete($id)
    {
        Category::where('id', $id)->delete();
        $this->rePosition();
        return json_encode('success');
    }


    public function up($id)
    {
        $this->action_name = 'up';
        $this->set_user_param();

        $this->rePosition();

        $category1 = Category::where('id', $id)->first();
        $position = $category1->position;
        $category2 = Category::where('position', $position - 1)->first();
        $category1->position = $position - 1;
        $category2->position = $position;
        $category1->save();
        $category2->save();

        return json_encode('success');

        //return redirect('/private/categories');
    }


    public function down($id)
    {
        $this->action_name = 'down';
        $this->set_user_param();

        $this->rePosition();

        $category1 = Category::where('id', $id)->first();
        $position = $category1->position;
        $category2 = Category::where('position', $position + 1)->first();
        $category1->position = $position + 1;
        $category2->position = $position;
        $category1->save();
        $category2->save();

        return json_encode('success');

        //return redirect('/private/categories');
    }


    public function rePosition()
    {
        $categories = Category::all()->sortBy('position');
        $i = 0;
        foreach ($categories as $category) {
            $i++;
            $category->position = $i;
            $category->save();
        }
    }


    public function getCategories()
    {

        $categories = Category::where('id', '>', 2)->orderBy('position')->get();
        //echo $categories;

        $returnHTML = view('admin.ajax.categories_tree')->with('categories', $categories)->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function getCategoriesSelect()
    {

        $categories = Category::where('id', '>', 2)->orderBy('position')->get();
        //echo $categories;

        $returnHTML = view('admin.ajax.categories_tree_select')->with('categories', $categories)->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function getCategoriesList()
    {

        $categories = Category::where('id', '>', 2)->orderBy('position')->get();

        $returnHTML = view('admin.ajax.categories_list')->with('categories', $categories)->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }


}
