<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Language;
use App\Address;
use App\AddressLanguage;

class AddressesController extends Controller
{
    public $controller_name = 'addresses';
    public $action_name = '';
    public $user;
    public $user_id = 0;
    public $user_name = '';

    private function set_user_param()
    {
        $this->user = Auth::user();
        $this->user_id = $this->user->id;
        $this->user_name = $this->user->name;
    }

    public function getResult()
    {
        $result = [
            'controller_name' => $this->controller_name,
            'action_name' => $this->action_name,
            'user_id' => $this->user_id,
            'user_name' => $this->user_name,
        ];

        return $result;
    }

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->action_name = 'index';
        $this->set_user_param();

        $result = $this->getResult();

        $addresses = Address::all()->sortBy('position');

        $result += [
            'addresses' => $addresses,
        ];

        return view('admin.addresses.index', $result);
    }

    public function edit(Request $request, $id)
    {
        $this->action_name = 'edit';
        $this->set_user_param();

        $result = $this->getResult();

        $status = $request->session()->get('status');;

        $languages = Language::all()->sortBy('position');

        foreach ($languages as $language) {
            $address_language = AddressLanguage::where('address_id', $id)->where('language_id', $language->id)->first();
            if ($address_language == null) {
                $address_language = new AddressLanguage([
                    'address_id' => $id,
                    'language_id' => $language->id,
                ]);
                $address_language->save();
            }
        }

        $address = Address::where('id', $id)->with('address_languages')->first();

        $result += [
            'languages' => $languages,
            'address' => $address,
        ];

        return view('admin.addresses.edit', $result);
    }


    public function update(Request $request)
    {
        $this->action_name = 'update';
        $this->set_user_param();

        $result = $this->getResult();

        $address_language = AddressLanguage::where('address_id', $request->input("address_id"))->where('language_id', $request->input("language_id"))->first();

        $address_language->text = $request->input("text");

        $address_language->save();

        return json_encode('success');
    }


    public function create()
    {
        $this->action_name = 'create';
        $this->set_user_param();

        $result = $this->getResult();

        return view('admin.addresses.create', $result);
    }


    public function add(Request $request)
    {

        $this->rePosition();
        $data = new Address();
        $data->name = $request->input("name");
        $max_position=Address::max('position');
        $data->position = $max_position+1;
        $data->save();
        return redirect('private/addresses');
    }


    public function delete($id)
    {
        Address::where('id', $id)->delete();
        $this->rePosition();
        return json_encode('success');
    }


    public function up($id)
    {
        $this->action_name = 'up';
        $this->set_user_param();

        $this->rePosition();

        $address1 = Address::where('id', $id)->first();
        $position=$address1->position;
        $address2 = Address::where('position', $position-1)->first();
        $address1->position=$position-1;
        $address2->position=$position;
        $address1->save();
        $address2->save();

        return redirect('/private/addresses');
    }


    public function down($id)
    {
        $this->action_name = 'down';
        $this->set_user_param();

        $this->rePosition();

        $address1 = Address::where('id', $id)->first();
        $position=$address1->position;
        $address2 = Address::where('position', $position+1)->first();
        $address1->position=$position+1;
        $address2->position=$position;
        $address1->save();
        $address2->save();

        return redirect('/private/addresses');
    }


    public function rePosition()
    {
        $addresses = Address::all()->sortBy('position');
        $i=0;
        foreach ($addresses as $address) {
            $i++;
            $address->position=$i;
            $address->save();
        }
    }


}
