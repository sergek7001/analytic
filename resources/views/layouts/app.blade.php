<!DOCTYPE html>
<<<<<<< HEAD
<html class="no-js" lang="de">
<head>
<?php if($_SERVER['HTTP_HOST'] != 'localhost') {?>
<!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-MQH4D79');</script>
    <!-- End Google Tag Manager -->
    <?php }?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,700,800">
    <link rel="stylesheet" href="css/main.css">
    <script src="scripts/vendor/modernizr.js"></script>

    <meta name="language" content="de"/>
    <meta name="robots" content="INDEX,FOLLOW"/>
    <meta name="format-detection" content="telephone=no">

    <meta name="google-site-verification" content="7p1MVKu9MFSzx2OxzL7zGJ1tDry8JZ_ZtRrB3idrxl8"/>

</head>
<body class="page">
<?php if($_SERVER['HTTP_HOST'] != 'localhost') {?>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MQH4D79"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<?php }?>

@include('partials.header')
@yield('content')

@include('partials.footer')
@include('partials.js')
<script type="text/javascript" 
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCkh4NXozZCwAVS2o4imqzCrqMoGTOoaw4&libraries=places"></script>
<!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
<script type="text/javascript">
    window.cookieconsent_options = {
        "message": "Diese Website verwendet Cookies, um die Nutzerfreundlichkeit zu verbessern. Durch die weitere Nutzung der Website stimmen Sie dem zu.",
        "dismiss": "Einverstanden",
        "learnMore": "Weitere Infos zu Cookies und deren Deaktivierung finden sie hier.",
        "link": "https://www.credicom.de/datenschutz.html",
        "theme": "light-bottom"
    };
</script>
<script src="/js/jquery-simulate-master/jquery.simulate.js"></script>
<script type="text/javascript"
        src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
<!-- End Cookie Consent plugin -->
<script>
    $(document).ready(function () {
        console.log('AUTOCOMPLETE CHECK')
        window.try = 'second';
        console.log('start.window.try', window.try);
    })
/* 	
// search input
const searchInput = document.getElementById('plz');
const arbeitInput = document.getElementById('arbeitgeber_plz');
const coapplicantInput = document.getElementById('coapplicant_plz');
// Google Maps autocomplete
const autocomplete = new google.maps.places.Autocomplete(searchInput, {componentRestrictions: {country: 'de'},
                types: ['(regions)']  });
				const arbeit_autocomplete = new google.maps.places.Autocomplete(arbeitInput, {componentRestrictions: {country: 'de'},
                types: ['(regions)']  });
				const coapplicant_autocomplete = new google.maps.places.Autocomplete(coapplicantInput, {componentRestrictions: {country: 'de'},
                types: ['(regions)']  });
// Has user pressed the down key to navigate autocomplete options?
let hasDownBeenPressed = false;
let arbeithasDownBeenPressed = false;
let coapplicantDownBeenPressed = false;
// Listener outside to stop nested loop returning odd results
searchInput.addEventListener('keydown', (e) => {
    if (e.keyCode === 40) {
        hasDownBeenPressed = true;
    }
});

arbeitInput.addEventListener('keydown', (e) => {
    if (e.keyCode === 40) {
        arbeithasDownBeenPressed = true;
    }
});

coapplicantInput.addEventListener('keydown', (e) => {
    if (e.keyCode === 40) {
        coapplicantDownBeenPressed = true;
    }
});

searchInput.addEventListener('mousedown', (e) => {
  window.try='first';
});
arbeitInput.addEventListener('mousedown', (e) => {
  window.try='first';
});
coapplicantInput.addEventListener('mousedown', (e) => {
  window.try='first';
});
google.maps.event.addDomListener(searchInput, 'mousedown', (e) => {
	console.log('EEE',searchInput.value)
})
google.maps.event.addDomListener(arbeitInput, 'mousedown', (e) => {
	console.log('EEE',arbeitInput.value)
})
google.maps.event.addDomListener(coapplicantInput, 'mousedown', (e) => {
	console.log('EEE',coapplicantInput.value)
})
var str='';
// GoogleMaps API custom eventlistener method
google.maps.event.addDomListener(searchInput, 'keydown', (e) => {
	
    // Maps API e.stopPropagation();
    e.cancelBubble = true;

    // If enter key, or tab key
if (e.keyCode === 13 || e.keyCode === 9 ) {
        // If user isn't navigating using arrows and this hasn't ran yet
        if (!hasDownBeenPressed && !e.hasRanOnce) {
            google.maps.event.trigger(e.target, 'keydown', {
                keyCode: 40,
                hasRanOnce: true,
            });
        }
		
		
    }

});

google.maps.event.addDomListener(arbeitInput, 'keydown', (e) => {
	
    // Maps API e.stopPropagation();
    e.cancelBubble = true;

    // If enter key, or tab key
if (e.keyCode === 13 || e.keyCode === 9 ) {
        // If user isn't navigating using arrows and this hasn't ran yet
        if (!arbeithasDownBeenPressed && !e.hasRanOnce) {
            google.maps.event.trigger(e.target, 'keydown', {
                keyCode: 40,
                hasRanOnce: true,
            });
        }
		
		
    }

});

google.maps.event.addDomListener(coapplicantInput, 'keydown', (e) => {
	
    // Maps API e.stopPropagation();
    e.cancelBubble = true;

    // If enter key, or tab key
if (e.keyCode === 13 || e.keyCode === 9 ) {
        // If user isn't navigating using arrows and this hasn't ran yet
        if (!coapplicanthasDownBeenPressed && !e.hasRanOnce) {
            google.maps.event.trigger(e.target, 'keydown', {
                keyCode: 40,
                hasRanOnce: true,
            });
        }
		
		
    }

});

 // Clear the input on focus, reset hasDownBeenPressed
searchInput.addEventListener('focus', () => {
    hasDownBeenPressed = false;
    searchInput.value = '';
});
arbeitInput.addEventListener('focus', () => {
    arbeithasDownBeenPressed = false;
    arbeitInput.value = '';
});
coapplicantInput.addEventListener('focus', () => {
    coapplicanthasDownBeenPressed = false;
    coapplicantInput.value = '';
});

   

$('#arbeitgeber_plz').change(function(){
	
	
// place_changed GoogleMaps listener when we do submit
google.maps.event.addListener(arbeit_autocomplete, 'place_changed', function() {

    // Get the place info from the autocomplete Api
    const arbeit_place = arbeit_autocomplete.getPlace();
console.log('PLACE',arbeit_place);
var plz = arbeit_place.address_components[0].short_name
var city = arbeit_place.address_components[2].short_name
$('#arbeitgeber_plz').val(plz);
$('#arbeitgeber_ort').val(city);
    //If we can find the place lets go to it
    if (typeof arbeit_place.address_components !== 'undefined') {          
        // reset hasDownBeenPressed in case they don't unfocus
        arbeithasDownBeenPressed = false;
    }

});
	
	  google.maps.event.trigger(arbeitInput, 'keydown',  {
	                keyCode: 40,hasRanOnce: true,
            });
	

//		
})

$('#coapplicant_plz').change(function(){
	
	
// place_changed GoogleMaps listener when we do submit
google.maps.event.addListener(coapplicant_autocomplete, 'place_changed', function() {

    // Get the place info from the autocomplete Api
    const coapplicant_place = coapplicant_autocomplete.getPlace();
console.log('PLACE',coapplicant_place);
var plz = coapplicant_place.address_components[0].short_name
var city = coapplicant_place.address_components[2].short_name
$('#coapplicant_plz').val(plz);
$('#coapplicant_ort').val(city);$('#coapplicant_str').focus();
    //If we can find the place lets go to it
    if (typeof coapplicant_place.address_components !== 'undefined') {          
        // reset hasDownBeenPressed in case they don't unfocus
        coapplicanthasDownBeenPressed = false;
    }

});
	
	  google.maps.event.trigger(coapplicantInput, 'keydown',  {
	                keyCode: 40,hasRanOnce: true,
            });
	

//		
})
   

function focus_(){
	$('#str').focus()
}
function focus_arbeit(){
	$('#at_tag').focus()
}
function focus_coapplicant(){
	$('#coapplicant_geb_tag').focus()
}

$('#plz').change(function(){
	
	
// place_changed GoogleMaps listener when we do submit
google.maps.event.addListener(autocomplete, 'place_changed', function() {

    // Get the place info from the autocomplete Api
    const place = autocomplete.getPlace();
console.log('PLACE',place);
var plz = place.address_components[0].short_name
var city = place.address_components[2].short_name
$('#plz').val(plz);
$('#ort').val(city);$('#str').focus();
    //If we can find the place lets go to it
    if (typeof place.address_components !== 'undefined') {          
        // reset hasDownBeenPressed in case they don't unfocus
        hasDownBeenPressed = false;
    }

});
	
	  google.maps.event.trigger(searchInput, 'keydown',  {
	                keyCode: 40,hasRanOnce: true,
            });
	

//		
})


$('#plz').keydown(	function(event){
	console.log(event)
	var str = $('#plz').val()
	console.log(str.length)
	if(str.length==4){
		setTimeout(focus_, 500)
		
	}
	
	
})

$('#arbeitgeber_plz').keydown(	function(event){
	console.log(event)
	var str = $('#arbeitgeber_plz').val()
	console.log(str.length)
	if(str.length==4){
		setTimeout(focus_arbeit, 500)
		
	}
	
	
})
$('#coapplicant_plz').keydown(	function(event){
	console.log(event)
	var str = $('#coapplicant_plz').val()
	console.log(str.length)
	if(str.length==4){
		setTimeout(focus_coapplicant, 500)
		
	}
	
	
})
 */

</script>

<script>
    $('.creditform-mobile').click(function () {
        $('html, body').animate({
            scrollTop: $(".label-greentext").offset().top
        }, 2000);
    })
</script>

<style>
    .pac-container {
        background-color: #fff;
        position: absolute !important;
        z-index: 1000;
        border-radius: 2px;
        border-top: 1px solid #d9d9d9;
        font-family: Arial, sans-serif;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        overflow: hidden;
        min-width: 150px;
    }

    .pac-container:after {
        /* Disclaimer: not needed to show 'powered by Google' if also a Google Map is shown */

        background-image: none !important;
        height: 0px;
    }

</style>
<!--test all-->
</body>
</html>
=======
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <li class="nav-item">
                                @if (Route::has('register'))
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                @endif
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
>>>>>>> bb2e14deeb4d633551f707afda5306b9477d33d2
