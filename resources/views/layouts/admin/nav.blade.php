


<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<aside id="left-panel">

    <!-- User info -->
    <div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as it -->

					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
						<img src="<?=asset('img/avatars/sunny.png')?>" alt="me" class="online"/>
						<span>
							{{isset($user_name)?$user_name:''}}
						</span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
    </div>
    <!-- end user info -->

    <!-- NAVIGATION : This navigation is also responsive-->
    <nav>

        <ul>
        <!--
            <li>
                <a href="/personal"><i class="fa fa-lg fa-fw fa-inbox"></i> <span
                            class="menu-item-parent">{{trans('personal_nav.home')}}</span></a>
            </li>
            -->

            <li class="{{ $controller_name=='start'?'active':'inactive' }}">
                <a href="/admin"><i class="fa fa-lg fa-fw fa-home"></i> <span
                            class="menu-item-parent">Home</span></a>
            </li>

            <li class="{{ $controller_name=='estates'?'active':'inactive' }}">
                <a href="#"><i class="fa fa-lg fa-fw fa-group"></i>
                    <span class="menu-item-parent">Real Estate</span>
                </a>
                <ul>
                    <li class="{{ $controller_name=='estates'?'active':'inactive' }}">
                        <a href="/admin/estates/"><i class="fa fa-lg fa-fw fa-map"></i>
                            <span class="menu-item-parent">Estates</span>
                        </a>
                    </li>

                    <li class="{{ $controller_name=='premoderation'?'active':'inactive' }}">
                        <a href="/admin/estate/moderation-create"><i class="fa fa-lg fa-fw fa-map"></i>
                            <span class="menu-item-parent">Moderation create</span>
                        </a>
                    </li>
                    <!--<li class="{{ $controller_name=='moderation'?'active':'inactive' }}">
                        <a href="/admin/estate/moderation-change"><i class="fa fa-lg fa-fw fa-map"></i>
                            <span class="menu-item-parent">Moderation change</span>
                        </a>
                    </li>

                    <li class="{{ $controller_name=='paused'?'active':'inactive' }}">
                        <a href="/admin/estate/paused"><i class="fa fa-lg fa-fw fa-map"></i>
                            <span class="menu-item-parent">Paused</span>
                        </a>
                    </li>

                    <li class="{{ $controller_name=='deleted'?'active':'inactive' }}">
                        <a href="/admin/estate/deleted"><i class="fa fa-lg fa-fw fa-map"></i>
                            <span class="menu-item-parent">Deleted</span>
                        </a>
                    </li>-->
                </ul>
            </li>

            <li class="{{ $controller_name=='categories'?'active':'inactive' }}">
                <a href="/admin/categories"><i class="fa fa-lg fa-fw fa-language "></i>
                    <span class="menu-item-parent">Categories</span>
                </a>
            </li>

            <li class="{{ $controller_name=='messages'?'active':'inactive' }}">
                <a href="#"><i class="fa fa-lg fa-fw fa-group"></i>
                    <span class="menu-item-parent">Messages</span>
                </a>
                <ul>
                    <li class="{{ $controller_name=='estate_message'?'active':'inactive' }}">
                        <a href="/admin/messages/estate"><i class="fa fa-lg fa-fw fa-map"></i>
                            <span class="menu-item-parent">Estate message</span>
                        </a>
                    </li>

                    <li class="{{ $controller_name=='contact_message'?'active':'inactive' }}">
                        <a href="/admin/messages/contact"><i class="fa fa-lg fa-fw fa-map"></i>
                            <span class="menu-item-parent">Contact message</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="{{ $controller_name=='tag'?'active':'inactive' }}">
                <a href="/admin/tags"><i class="fa fa-lg fa-fw fa-bookmark-o"></i>
                    <span class="menu-item-parent">Tags</span>
                </a>
            </li>

            <li class="{{ $controller_name=='pages'?'active':'inactive' }}">
                <a href="/admin/pages"><i class="fa fa-lg fa-fw fa-bookmark-o"></i>
                    <span class="menu-item-parent">Site Pages</span>
                </a>
            </li>

            <li class="{{ $controller_name=='articles'?'active':'inactive' }}">
                <a href="/admin/articles"><i class="fa fa-lg fa-fw fa-building"></i>
                    <span class="menu-item-parent">Articles</span>
                </a>
            </li>

            <li>
                <a href="#" title="Dashboard"><i class="fa fa-lg fa-fw fa-cubes"></i> <span
                            class="menu-item-parent">Admins</span></a>
                <ul>

                    <li class="{{ $controller_name=='permissions'?'active':'inactive' }}">
                        <a href="/admin/permissions"><i class="fa fa-lg fa-fw fa-map"></i> <span
                                    class="menu-item-parent">Permissions</span>
                        </a>
                    </li>
                    <li class="{{ $controller_name=='admins'?'active':'inactive' }}">
                        <a href="/admin/users"><i class="fa fa-lg fa-fw fa-check-circle"></i>
                            <span class="menu-item-parent">Users</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="{{ $controller_name=='articles'?'active':'inactive' }}">
                <a href="/admin/contacts_info"><i class="fa fa-lg fa-fw fa-pencil-square-o"></i>
                    <span class="menu-item-parent">Contact Info</span>
                </a>
            </li>
        </ul>

    </nav>


    <span class="minifyme" data-action="minifyMenu">
				<i class="fa fa-arrow-circle-left hit"></i> 
			</span>

</aside>
<!-- END NAVIGATION -->