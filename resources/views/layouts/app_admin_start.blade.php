<!DOCTYPE html>
<html lang="en-us">
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

    <title> SmartAdmin </title>
    <meta name="description" content="">
    <meta name="author" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="/smartAdmin/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/smartAdmin/css/font-awesome.min.css">

    <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
    <link rel="stylesheet" type="text/css" media="screen" href="/smartAdmin/css/smartadmin-production-plugins.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/smartAdmin/css/smartadmin-production.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="/smartAdmin/css/smartadmin-skins.min.css">

    <!-- SmartAdmin RTL Support -->
    <link rel="stylesheet" type="text/css" media="screen" href="/smartAdmin/css/smartadmin-rtl.min.css">

    <!-- We recommend you use "your_style.css" to override SmartAdmin
         specific styles this will also ensure you retrain your customization with each SmartAdmin update.
    <link rel="stylesheet" type="text/css" media="screen" href="/smartAdmin/css/your_style.css"> -->

    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="/smartAdmin/css/demo.min.css">

    <!-- FAVICONS -->
    <link rel="shortcut icon" href="img/favicon/favicon.ico" type="image/x-icon">
    <link rel="icon" href="img/favicon/favicon.ico" type="image/x-icon">

    <!-- GOOGLE FONT -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

    <!-- Specifying a Webpage Icon for Web Clip 
         Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
    <link rel="apple-touch-icon" href="img/splash/sptouch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/splash/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/splash/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/splash/touch-icon-ipad-retina.png">

</head>

<!--

TABLE OF CONTENTS.

Use search to find needed section.

===================================================================

|  01. #CSS Links                |  all CSS links and file paths  |
|  02. #FAVICONS                 |  Favicon links and file paths  |
|  03. #GOOGLE FONT              |  Google font link              |
|  04. #APP SCREEN / ICONS       |  app icons, screen backdrops   |
|  05. #BODY                     |  body tag                      |
|  06. #HEADER                   |  header tag                    |
|  07. #PROJECTS                 |  project lists                 |
|  08. #TOGGLE LAYOUT BUTTONS    |  layout buttons and actions    |
|  09. #MOBILE                   |  mobile view dropdown          |
|  10. #SEARCH                   |  search field                  |
|  11. #NAVIGATION               |  left panel & navigation       |
|  12. #RIGHT PANEL              |  right panel userlist          |
|  13. #MAIN PANEL               |  main panel                    |
|  14. #MAIN CONTENT             |  content holder                |
|  15. #PAGE FOOTER              |  page footer                   |
|  16. #SHORTCUT AREA            |  dropdown shortcuts area       |
|  17. #PLUGINS                  |  all scripts and plugins       |

===================================================================

-->

<!-- #BODY -->
<!-- Possible Classes

    * 'smart-style-{SKIN#}'
    * 'smart-rtl'         - Switch theme mode to RTL
    * 'menu-on-top'       - Switch to top navigation (no DOM change required)
    * 'no-menu'			  - Hides the menu completely
    * 'hidden-menu'       - Hides the main menu but still accessable by hovering over left edge
    * 'fixed-header'      - Fixes the header
    * 'fixed-navigation'  - Fixes the main menu
    * 'fixed-ribbon'      - Fixes breadcrumb
    * 'fixed-page-footer' - Fixes footer
    * 'container'         - boxed layout mode (non-responsive: will not work with fixed-navigation & fixed-ribbon)
-->
<body class="">

<!-- HEADER -->
@include ('partials.admin_header');
<!-- END HEADER -->

<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
@include('partials.navigation');
<!-- END NAVIGATION -->

<!-- MAIN PANEL -->
@yield('content')
<!-- END MAIN PANEL -->

<!-- PAGE FOOTER -->
@include ('partials.footer_admin');
<!-- END PAGE FOOTER -->

<!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
Note: These tiles are completely responsive,
you can add as many as you like
-->
<div id="shortcut">
    <ul>
        <li>
            <a href="inbox.html" class="jarvismetro-tile big-cubes bg-color-blue"> <span class="iconbox"> <i class="fa fa-envelope fa-4x"></i> <span>Mail <span class="label pull-right bg-color-darken">14</span></span> </span> </a>
        </li>
        <li>
            <a href="calendar.html" class="jarvismetro-tile big-cubes bg-color-orangeDark"> <span class="iconbox"> <i class="fa fa-calendar fa-4x"></i> <span>Calendar</span> </span> </a>
        </li>
        <li>
            <a href="gmap-xml.html" class="jarvismetro-tile big-cubes bg-color-purple"> <span class="iconbox"> <i class="fa fa-map-marker fa-4x"></i> <span>Maps</span> </span> </a>
        </li>
        <li>
            <a href="invoice.html" class="jarvismetro-tile big-cubes bg-color-blueDark"> <span class="iconbox"> <i class="fa fa-book fa-4x"></i> <span>Invoice <span class="label pull-right bg-color-darken">99</span></span> </span> </a>
        </li>
        <li>
            <a href="gallery.html" class="jarvismetro-tile big-cubes bg-color-greenLight"> <span class="iconbox"> <i class="fa fa-picture-o fa-4x"></i> <span>Gallery </span> </span> </a>
        </li>
        <li>
            <a href="profile.html" class="jarvismetro-tile big-cubes selected bg-color-pinkDark"> <span class="iconbox"> <i class="fa fa-user fa-4x"></i> <span>My Profile </span> </span> </a>
        </li>
    </ul>
</div>
<!-- END SHORTCUT AREA -->

<!--================================================== -->

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="/smartAdmin/js/plugin/pace/pace.min.js"></script>

<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    if (!window.jQuery) {
        document.write('<script src="/smartAdmin/js/libs/jquery-3.2.1.min.js"><\/script>');
    }
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script>
    if (!window.jQuery.ui) {
        document.write('<script src="/smartAdmin/js/libs/jquery-ui.min.js"><\/script>');
    }
</script>

<!-- IMPORTANT: APP CONFIG -->
<script src="/smartAdmin/js/app.config.js"></script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="/smartAdmin/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script>

<!-- BOOTSTRAP JS -->
<script src="/smartAdmin/js/bootstrap/bootstrap.min.js"></script>

<!-- CUSTOM NOTIFICATION -->
<script src="/smartAdmin/js/notification/SmartNotification.min.js"></script>

<!-- JARVIS WIDGETS -->
<script src="/smartAdmin/js/smartwidgets/jarvis.widget.min.js"></script>

<!-- EASY PIE CHARTS -->
<script src="/smartAdmin/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"></script>

<!-- SPARKLINES -->
<script src="/smartAdmin/js/plugin/sparkline/jquery.sparkline.min.js"></script>

<!-- JQUERY VALIDATE -->
<script src="/smartAdmin/js/plugin/jquery-validate/jquery.validate.min.js"></script>

<!-- JQUERY MASKED INPUT -->
<script src="/smartAdmin/js/plugin/masked-input/jquery.maskedinput.min.js"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="/smartAdmin/js/plugin/select2/select2.min.js"></script>

<!-- JQUERY UI + Bootstrap Slider -->
<script src="/smartAdmin/js/plugin/bootstrap-slider/bootstrap-slider.min.js"></script>

<!-- browser msie issue fix -->
<script src="/smartAdmin/js/plugin/msie-fix/jquery.mb.browser.min.js"></script>

<!-- FastClick: For mobile devices -->
<script src="/smartAdmin/js/plugin/fastclick/fastclick.min.js"></script>

<!--[if IE 8]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

<![endif]-->

<!-- Demo purpose only -->
<script src="/smartAdmin/js/demo.min.js"></script>

<!-- MAIN APP JS FILE -->
<script src="/smartAdmin/js/app.min.js"></script>

<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="/smartAdmin/js/speech/voicecommand.min.js"></script>

<!-- SmartChat UI : plugin -->
<script src="/smartAdmin/js/smart-chat-ui/smart.chat.ui.min.js"></script>
<script src="/smartAdmin/js/smart-chat-ui/smart.chat.manager.min.js"></script>

<!-- PAGE RELATED PLUGIN(S) -->

<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
<script src="/smartAdmin/js/plugin/flot/jquery.flot.cust.min.js"></script>
<script src="/smartAdmin/js/plugin/flot/jquery.flot.resize.min.js"></script>
<script src="/smartAdmin/js/plugin/flot/jquery.flot.time.min.js"></script>
<script src="/smartAdmin/js/plugin/flot/jquery.flot.tooltip.min.js"></script>



<script>
    $(document).ready(function() {
	$('#preloader_background').css('display','none');
		$('#preloader').css('display','none');
        // DO NOT REMOVE : GLOBAL FUNCTIONS!
        pageSetUp();

        /*
         * PAGE RELATED SCRIPTS
         */

        $(".js-status-update a").click(function() {
            var selText = $(this).text();
            var $this = $(this);
            $this.parents('.btn-group').find('.dropdown-toggle').html(selText + ' <span class="caret"></span>');
            $this.parents('.dropdown-menu').find('li').removeClass('active');
            $this.parent().addClass('active');
        });

        /*
         * TODO: add a way to add more todo's to list
         */

        // initialize sortable
        $(function() {
            $("#sortable1, #sortable2").sortable({
                handle : '.handle',
                connectWith : ".todo",
                update : countTasks
            }).disableSelection();
        });

        // check and uncheck
        $('.todo .checkbox > input[type="checkbox"]').click(function() {
            var $this = $(this).parent().parent().parent();

            if ($(this).prop('checked')) {
                $this.addClass("complete");

                // remove this if you want to undo a check list once checked
                //$(this).attr("disabled", true);
                $(this).parent().hide();

                // once clicked - add class, copy to memory then remove and add to sortable3
                $this.slideUp(500, function() {
                    $this.clone().prependTo("#sortable3").effect("highlight", {}, 800);
                    $this.remove();
                    countTasks();
                });
            } else {
                // insert undo code here...
            }

        })
        // count tasks
        function countTasks() {

            $('.todo-group-title').each(function() {
                var $this = $(this);
                $this.find(".num-of-tasks").text($this.next().find("li").size());
            });

        }

        /*
         * RUN PAGE GRAPHS
         */

        /* TAB 1: UPDATING CHART */
        // For the demo we use generated data, but normally it would be coming from the server
/*

        var data = [], totalPoints = 200, $UpdatingChartColors = $("#updating-chart").css('color');

        function getRandomData() {
            if (data.length > 0)
                data = data.slice(1);

            // do a random walk
            while (data.length < totalPoints) {
                var prev = data.length > 0 ? data[data.length - 1] : 50;
                var y = prev + Math.random() * 10 - 5;
                if (y < 0)
                    y = 0;
                if (y > 100)
                    y = 100;
                data.push(y);
            }

            // zip the generated y values with the x values
            var res = [];
            for (var i = 0; i < data.length; ++i)
                res.push([i, data[i]])
            return res;
        }
var _data=[];
        function setGraphData(totalPoints) {
            ordinat=[2000,300,2000,500,3000,200,800];
            //Сюда надо передать семь пар [0]=>0,[1]=>200,...[6]=>3000
            //if (_data.length > 0)
                //_data = _data.slice(1);
            alert('ordinat.length')
alert(ordinat.length)
            // do a random walk
            while (_data.length < totalPoints) {
                alert('_data.length');
                alert(_data.length);
                //var prev = data.length > 0 ? data[data.length - 1] : 50;
                //var y = prev + Math.random() * 10 - 5;
                alert(ordinat[_data.length])
                var y = ordinat[_data.length];
                if (y < 0)
                    y = 0;
                if (y > 3333)
                    y = 3333;
                _data.push(y);
            }

            // zip the generated y values with the x values
            var res = [];
            for (var i = 0; i < ordinat.length; ++i)

                res.push([i+1, _data[i]])
            return res;
        }

        // setup control widget
        var updateInterval = 1500;
        $("#updating-chart").val(updateInterval).change(function() {

            var v = $(this).val();
            if (v && !isNaN(+v)) {
                updateInterval = +v;
                $(this).val("" + updateInterval);
            }

        });

        // setup plot
        var options = {
            yaxis : {
                min : 0,
                max : 100
            },
            xaxis : {
                mode : "time",
                min : 0,
                max : 100
            },
            colors : [$UpdatingChartColors],
            series : {
                lines : {
                    lineWidth : 1,
                    fill : true,
                    fillColor : {
                        colors : [{
                            opacity : 0.4
                        }, {
                            opacity : 0
                        }]
                    },
                    steps : false

                }
            }
        };

        var plot = $.plot($("#updating-chart"), [getRandomData()], options);

        /!* live switch *!/
        $('input[type="checkbox"]#start_interval').click(function() {
            if ($(this).prop('checked')) {
                $on = true;
                updateInterval = 1500;
                update();
            } else {
                clearInterval(updateInterval);
                $on = false;
            }
        });
*/

        /*function update() {
            if ($on == true) {
                plot.setData([getRandomData()]);
                plot.draw();
                setTimeout(update, updateInterval);

            } else {
                clearInterval(updateInterval)
            }

        }*/

/*        function update(totalPoints) {
  alert(setGraphData(totalPoints))
            if ($on == true) {
                plot.setData([setGraphData(totalPoints)]);
                plot.draw();
                setTimeout(update(totalPoints), updateInterval);

            } else {
                clearInterval(updateInterval)
            }

        }*/

/*        function updateStatusGraph(count_days,max_summ_in_period,request){
            var options = {
                yaxis : {
                    min : 0,
                    max : max_summ_in_period
                },
                xaxis : {
                    min : 1,
                    max : count_days
                },
                colors : [$UpdatingChartColors],
                series : {
                    lines : {
                        lineWidth : 1,
                        fill : true,
                        fillColor : {
                            colors : [{
                                opacity : 0.4
                            }, {
                                opacity : 0
                            }]
                        },
                        steps : false

                    }
                }
            };
var totalPoints=7;
var _data=[];
            var plot = $.plot($("#updating-chart"), [setGraphData(totalPoints)], options);


            // setup control widget
            var updateInterval = 1500;
            $("#updating-chart").val(updateInterval).change(function() {

                var v = $(this).val();
                if (v && !isNaN(+v)) {
                    updateInterval = +v;
                    $(this).val("" + updateInterval);
                }

            });
            update(totalPoints);
        }*/

        var $on = false;

        /*end updating chart*/


        /* chart colors default */
        var $chrt_border_color = "#efefef";
        var $chrt_grid_color = "#DDD"
        var $chrt_main = "#E24913";
        /* red       */
        var $chrt_second = "#6595b4";
        /* blue      */
        var $chrt_third = "#FF9F01";
        /* orange    */
        var $chrt_fourth = "#7e9d3a";
        /* green     */
        var $chrt_fifth = "#BD362F";
        /* dark red  */
        var $chrt_mono = "#000";


        /* sales chart */
function setStatusGraph(d){
	$('#preloader_background').css('display','block');
		$('#preloader').css('display','block');
        if ($("#saleschart").length) {

            for (var i = 0; i < d.length; ++i){
                d[i][0] += 60 * 60 * 1000;
            }
            function weekendAreas(axes) {
                var markings = [];

                var d = new Date(axes.xaxis.min);
                // go to the first Saturday
                d.setUTCDate(d.getUTCDate() - ((d.getUTCDay() + 1) % 7))
                d.setUTCSeconds(0);
                d.setUTCMinutes(0);
                d.setUTCHours(0);
                var i = d.getTime();
                do {
                    // when we don't set yaxis, the rectangle automatically
                    // extends to infinity upwards and downwards
                    markings.push({
                        xaxis : {
                            from : i,
                            to : i + 2 * 24 * 60 * 60 * 1000
                        }
                    });
                    i += 7 * 24 * 60 * 60 * 1000;
                } while (i < axes.xaxis.max);

                return markings;
            }

            var options = {
                xaxis : {
                    mode : "time",
                    tickLength : 5
                },
                series : {
                    lines : {
                        show : true,
                        lineWidth : 1,
                        fill : true,
                        fillColor : {
                            colors : [{
                                opacity : 0.1
                            }, {
                                opacity : 0.15
                            }]
                        }
                    },
                    //points: { show: true },
                    shadowSize : 0
                },
                selection : {
                    mode : "x"
                },
                grid : {
                    hoverable : true,
                    clickable : true,
                    tickColor : $chrt_border_color,
                    borderWidth : 0,
                    borderColor : $chrt_border_color,
                },
                tooltip : true,
                tooltipOpts : {
                    content : "Your sales for <b>%x</b> was <span>$%y</span>",
                    dateFormat : "%y-%m-%d",
                    defaultTheme : false
                },
                colors : [$chrt_second],

            };

            var plot = $.plot($("#saleschart"), [d], options);
        };

        /* end sales chart */
		setTimeout(
				preloader_hide		
				, 2000);
}






        /* TAB 2: Social Network  */

        $(function() {
            // jQuery Flot Chart
            var twitter = [[1, 27], [2, 34], [3, 51], [4, 48], [5, 55], [6, 65], [7, 61], [8, 70], [9, 65], [10, 75], [11, 57], [12, 59], [13, 62]], facebook = [[1, 25], [2, 31], [3, 45], [4, 37], [5, 38], [6, 40], [7, 47], [8, 55], [9, 43], [10, 50], [11, 47], [12, 39], [13, 47]], data = [{
                label : "Twitter",
                data : twitter,
                lines : {
                    show : true,
                    lineWidth : 1,
                    fill : true,
                    fillColor : {
                        colors : [{
                            opacity : 0.1
                        }, {
                            opacity : 0.13
                        }]
                    }
                },
                points : {
                    show : true
                }
            }, {
                label : "Facebook",
                data : facebook,
                lines : {
                    show : true,
                    lineWidth : 1,
                    fill : true,
                    fillColor : {
                        colors : [{
                            opacity : 0.1
                        }, {
                            opacity : 0.13
                        }]
                    }
                },
                points : {
                    show : true
                }
            }];

            var options = {
                grid : {
                    hoverable : true
                },
                colors : ["#568A89", "#3276B1"],
                tooltip : true,
                tooltipOpts : {
                    //content : "Value <b>$x</b> Value <span>$y</span>",
                    defaultTheme : false
                },
                xaxis : {
                    ticks : [[1, "JAN"], [2, "FEB"], [3, "MAR"], [4, "APR"], [5, "MAY"], [6, "JUN"], [7, "JUL"], [8, "AUG"], [9, "SEP"], [10, "OCT"], [11, "NOV"], [12, "DEC"], [13, "JAN+1"]]
                },
                yaxes : {

                }
            };

            var plot3 = $.plot($("#statsChart"), data, options);
        });

        // END TAB 2

        // TAB THREE GRAPH //
        /* TAB 3: Revenew  */

        $(function() {

            var trgt = [[1354586000000, 153], [1364587000000, 658], [1374588000000, 198], [1384589000000, 663], [1394590000000, 801], [1404591000000, 1080], [1414592000000, 353], [1424593000000, 749], [1434594000000, 523], [1444595000000, 258], [1454596000000, 688], [1464597000000, 364]], prft = [[1354586000000, 53], [1364587000000, 65], [1374588000000, 98], [1384589000000, 83], [1394590000000, 980], [1404591000000, 808], [1414592000000, 720], [1424593000000, 674], [1434594000000, 23], [1444595000000, 79], [1454596000000, 88], [1464597000000, 36]], sgnups = [[1354586000000, 647], [1364587000000, 435], [1374588000000, 784], [1384589000000, 346], [1394590000000, 487], [1404591000000, 463], [1414592000000, 479], [1424593000000, 236], [1434594000000, 843], [1444595000000, 657], [1454596000000, 241], [1464597000000, 341]], toggles = $("#rev-toggles"), target = $("#flotcontainer");

            var data = [{
                label : "Target Profit",
                data : trgt,
                bars : {
                    show : true,
                    align : "center",
                    barWidth : 30 * 30 * 60 * 1000 * 80
                }
            }, {
                label : "Actual Profit",
                data : prft,
                color : '#3276B1',
                lines : {
                    show : true,
                    lineWidth : 3
                },
                points : {
                    show : true
                }
            }, {
                label : "Actual Signups",
                data : sgnups,
                color : '#71843F',
                lines : {
                    show : true,
                    lineWidth : 1
                },
                points : {
                    show : true
                }
            }]

            var options = {
                grid : {
                    hoverable : true
                },
                tooltip : true,
                tooltipOpts : {
                    //content: '%x - %y',
                    //dateFormat: '%b %y',
                    defaultTheme : false
                },
                xaxis : {
                    mode : "time"
                },
                yaxes : {
                    tickFormatter : function(val, axis) {
                        return "$" + val;
                    },
                    max : 1200
                }

            };

            plot2 = null;

            function plotNow() {
                var d = [];
                toggles.find(':checkbox').each(function() {
                    if ($(this).is(':checked')) {
                        d.push(data[$(this).attr("name").substr(4, 1)]);
                    }
                });
                if (d.length > 0) {
                    if (plot2) {
                        plot2.setData(d);
                        plot2.draw();
                    } else {
                        plot2 = $.plot(target, d, options);
                    }
                }

            };

            toggles.find(':checkbox').on('change', function() {
                plotNow();
            });
            plotNow()

        });

		function in_array(array, timestamp) {
	if(array.length > 0){
    for(var i=0;i<array.length;i++) {
		if(array[i].time === timestamp){
			if(array[i].price){
				var element=[]
				element[0]=array[i].price;
				element[1]=array[i].time;
				return element
				
				}
			else{
				return true;
			}
			
			
		}
        
    }
	}
    return false;
}
Object.size = function(obj) {
var size = 0, key;
for (key in obj) {
if (obj.hasOwnProperty(key)) size++;
}
return size;
};



        function getD(obj,id,date_range){
			console.log('obj=>',obj);
			console.log('DAT');
			console.log(date_range)
            var data_d=[];
            //распарсить массив дат date_range в строковых значениях дат
            //если дата пустая

                           




                                       $.each( date_range, function( kr,time ) {
							
											$.each( obj, function( key, value ) {
												
												var size = Object.size(value);
																		
												if(size>0){
													$.each( value, function( k, v ) {
														
														if(key==id){
															
															 var timeStamp = function(str) {
																 console.log('str=>',str);
																	return new Date(str.replace(/^(\d{2}\-)(\d{2}\-)(\d{4})$/,
																		'$2$1$3')).getTime();
																};
																var datum=v.created_at.split(' ');
																timestamp=timeStamp(datum[0]);
															 
															  var result = in_array(data_d, time);
															if(!result){
															 if(time==timestamp){
																data_d.push({time:timestamp,price:v.kreditbetrag});
																}
																else{
																	data_d.push({time:time,price:0});
																}
															}
															else{
																if(result===true){
																	console.log('true'+'=>'+result);
																	 if(time==timestamp){
																for (var index = 0; index < data_d.length; index++) {
																		if(data_d[index].time==time){
																			data_d[index].price=v.kreditbetrag
																		}
																		
																	 } }
																}
																else{
																	
																	if(result[1]===timestamp){
																		var s=v.kreditbetrag+result[0];
																	console.log('else'+'=>'+result[0] +'=>'+result[1]+'also have new value'+v.kreditbetrag);
																	//засунуть вместо таймстемпа сумму
																	for (var index = 0; index < data_d.length; index++) {
																		if(data_d[index].time==result[1]){
																			data_d[index].price=s
																		}
																		
																	}
																	}
																	
																}
															
																
															}															
																
																
																
														}
												});		
												}	
											});												
                                        });


console.log(data_d)
var dat=[];
 $.each( data_d, function( key,val ) {
	 dat.push([val.time,val.price]);
	 
 });
        return dat;
        }

		
/*         function getD(obj,id,date_range){
			console.log(date_range)
            var data_d=[];
            //распарсить массив дат date_range в строковых значениях дат
            //если дата пустая
            $.each( obj, function( key, value ) {

                if(value.length>0){
                    $.each( value, function( k, v ) {
                        if(key==id){
							alert(v.kreditbetrag)
                            var timeStamp = function(str) {
                                return new Date(str.replace(/^(\d{2}\-)(\d{2}\-)(\d{4})$/,
                                    '$2$1$3')).getTime();
                            };
                            timestamp=timeStamp(v.datum);




                                       $.each( date_range, function( kr,time ) {
										  // console.log('time'+'=>'+time)
										   var result = in_array(data_d, time);
										   if(!result){
                                           if(time==timestamp){
                                            data_d.push({time:timestamp,price:v.kreditbetrag});
                                            }
                                            else{
                                                data_d.push({time:time,price:0});
                                            }
											}
											else{
												if(result===true){
													console.log('true'+'=>'+result);
												}
												else{
													console.log('else'+'=>'+result);
												}
												//переписать
												
											}
                                        });


                        }

                    });

                }

            });
console.log(data_d)
        return data_d;
        } */


        function daysInMonth(month, year) {
            return new Date(year, month, 0).getDate();
        }
		$(document).ready(function(){
			console.log(789);
			$('input[name="statistic"]:first').attr('checked', true).trigger('click');
			var statistic_name=$('input[name="statistic"]:first').parent('li').find('label').text()
        $('.statistic_span').text(statistic_name);
		$("#statistic_form").submit();
		})
        $('#statistic_form').submit(function(){
		console.log('preloader');
				$('#preloader_background').css('display','block');
				$('#preloader').css('display','block');
            var timeStamp = function(str) {
                return new Date(str.replace(/^(\d{2}\-)(\d{2}\-)(\d{4})$/,
                    '$2$1$3')).getTime();
            };
            today=$.datepicker.formatDate('yy-mm-dd', new Date());
            var _MS_PER_DAY = 1000 * 60 * 60 * 24;
            var utc1 = timeStamp(today);

            var statistic_type=$('input[name="statistic"]:checked').val();
            var date_eingabe=$('input[name="date_eingabe"]').val();
            var d = new Date();
            var year = $.datepicker.formatDate('yy', new Date());
            var days_in_current_month=$.datepicker.formatDate('dd', new Date());
            var current_month = d.getMonth()+1;
            var current_year = d.getYear();
            var last_month = d.getMonth();
            if(last_month==0){
                last_month=12;
                year=year-1;
            }
            if(date_eingabe!==null){
            switch(statistic_type){
                case '0' : window.graph_period=null;break;
                case '1' : window.graph_period=7;


                break;
                case '2' :
                window.graph_period= days_in_current_month ;
                break; // current month
                case '3' :
                    window.graph_period= daysInMonth(last_month, year) ;
                break; // last month
            }
            }
            else{
                window.graph_period=null;
            }
            //alert(window.graph_period)
            window.graph_array=[];
            for (var i = 0; i <= window.graph_period; ++i){
                var num_days_ago=utc1-_MS_PER_DAY*(window.graph_period-i);
                window.graph_array.push(num_days_ago);
            }
            $.ajax({
                type: "POST",
                url: '/start/statistic_static',
                data: { statistic_type:statistic_type,date_eingabe:date_eingabe },
               // async: false,
                dataType: 'json',
				beforeSend: function() {
				
				},
			
                success: function(response) {

                    window.status_info=response.status;

                    console.log(response);
                     $('#partner_info').empty();
                    $('#total_anfragen').empty();
                    $('#total_kreditsumme').empty();
                    $.each( response.partner, function( key, value ) {

                        $('#partner_info').append(
                            '<tr>'+
                            '<th> '+value.partner_id+'</th>'+
                            '<th> '+value.anfragen+'</th>'+
                            '<th> '+value.kreditsumme+'</th>'+
                            '</tr>'
                        );

                    });
                    $('#total_anfragen').append(response.total.anfragen);
					$('.count_of_creditorders').empty()
					$('.count_of_creditorders').append(response.total.anfragen);
                    $('#total_kreditsumme').append(response.total.kreditsumme);



                    $('#status_info').empty();
                    $('#status_lines').empty();
                    $.each( response.status, function( key, value ) {
                        if(key!=='for_graph' ){

                        switch (value.status){
                            case 'negativ': color='danger'; bg_color='bg-color-red';break;
                            case 'positive': color='success';bg_color='bg-color-green';break;
                            case 'neitral': color='info';bg_color='bg-color-blue';break;
                        }
                        $('#status_info').append(
                            '<tr class="'+color+'">'+
                            '<th> '+key+'</th>'+
                            '<th> '+value.count+'</th>'+
                            '<th> '+value.summe+'</th>'+
                            '</tr>'
                        );
                        if(value.summe>0){
                        $('#status_lines').append(
                       '<div class="col-xs-6 col-sm-6 col-md-12 col-lg-12"> ' +
                       '<span style="display:inline-block"><a class="btn btn-primary graph_btn" href="javascript:void(0);"><i class="fa fa-bar-chart-o"></i>'+
                        '<input type="hidden" class="graph_btn_id" value="'+value.id+'">'+
                       '<input type="hidden" class="graph_btn_summ" value="'+value.summe+'">'+
                       '</a></span>' +

                       '<span style="padding-left:20px" class="text">'+key+'<span class="pull-right">'+value.summe+'/'+response.total.kreditsumme+'</span> </span>'+
                            '<div class="progress">'+
                            '<div class="progress-bar '+bg_color+'" style="width: '+value.summe/response.total.kreditsumme*100+'%;"></div>'+
                            '</div> </div>'
                        );
                        }

                        }
                    });






				
				
				       $('.graph_btn').click(function(){

                var graph_btn_id=$(this).find('.graph_btn_id').val();//этот айди не правильный вместо 28 надо давать 2
                var graph_btn_summ=$(this).find('.graph_btn_summ').val();
				for_graph=response.status.for_graph
				console.log('for_graph',for_graph);
                var d =getD(window.status_info.for_graph,graph_btn_id,window.graph_array);
				//Надо переделать что отображаются в шкале времени последние семь дней включая с сегоднишним
                
				
				setTimeout(
				setStatusGraph(d)		
				, 2000);
				
            })

                },	
				complete: function() {
				$('#preloader_background').css('display','none');
				$('#preloader').css('display','none');
				},
            });

     

        })

    });
function preloader_hide(){
			$('#preloader_background').css('display','none');
		$('#preloader').css('display','none');
}
</script>







<script>
    // DO NOT REMOVE : GLOBAL FUNCTIONS!
    $(document).ready(function() {
        $('#wiedervolage_response').empty();
        var wiedervolage_status=null;
        
		var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();
if(dd<10){
    dd='0'+dd;
} 
if(mm<10){
    mm='0'+mm;
} 
var today = yyyy+'/'+mm+'/'+dd;
		console.log(today );//2018/08/28
		var wiedervolage_date=today;
        var wiedervolage_user='current_user';


        $.ajax({
            type: "POST",
            url: "/<?php echo $admin_type;?>"+'/wiedervolage',
            data: { wiedervolage_status:wiedervolage_status,wiedervolage_date:wiedervolage_date,wiedervolage_user:wiedervolage_user },
            async: false,
            dataType: 'json',
            success: function(response) {

	console.log(response);
    $.each( response.credits, function( key, value ) {
					
switch(value.prio){
	case 0:
	console.log(value.prio);
	var prio='<img src="/img/prio0.png">';
	break;
	case 1:
	var prio='<img src="/img/prio1.png">';
	break;
	case 2:
	var prio='<img src="/img/prio2.png">';
	break;
	case 3:
	var prio='<img src="/img/prio3.png">';
	break;
}
                    $('#wiedervolage_response').append(
                        '<tr>'+
                        '<th> '+value.id+'</th>'+
                        '<th> '+value.date+'</th>'+
                        '<th> '+value.vorname+' '+value.nachname+'</th>'+
                        '<th> '+value.kreditbetrag+'</th>'+
                        '<th> '+value.status_intern+'</th>'+
                        '<th> '+value.wiedervorlage_user+'</th>'+
                        '<th> '+value.last_comment.inhalt+'</th>'+
                        '<th> <a class="btn btn-primary" target="_blank" href="/<?php echo $admin_type?>/credit/edit/'+value.id+'"><i class="fa fa-pencil"></i> </a></tr>'
                    );

                });









            }
        });

		
		
		
        pageSetUp();
    })

</script>

<script>
    $('input[name="wiedervolage_user"]').change(function(){
        var benutzer_name=$(this).parent('li').find('label').text()
        $('.benutzer_span').text(benutzer_name)
        $('.benutzer_caret').click();
    })

    $('input[name="wiedervolage_status"]').change(function(){
        var status_name=$(this).parent('li').find('label').text()
        $('.status_span').text(status_name)
        $('.status_caret').click();
    })

    $('input[name="statistic"]').change(function(){
        var statistic_name=$(this).parent('li').find('label').text()
        $('.statistic_span').text(statistic_name)
        $('.statistic_caret').click();
    })




</script>



<script>
    $('#wiedervolage_submit').click(function(){
        $('#wiedervolage_response').empty();
        var wiedervolage_status=$('input[name=wiedervolage_status]:checked').val();
        var wiedervolage_date=$('#wiedervolage_date').val();
        var wiedervolage_user=$('input[name=wiedervolage_user]:checked').val();

console.log( wiedervolage_date);
        $.ajax({
            type: "POST",
            url: "/<?php echo $admin_type;?>"+'/wiedervolage',
            data: { wiedervolage_status:wiedervolage_status,wiedervolage_date:wiedervolage_date,wiedervolage_user:wiedervolage_user },
            async: false,
            dataType: 'json',
            success: function(response) {

console.log(response);
                $.each( response.credits, function( key, value ) {
					
switch(value.prio){
	case 0:
	console.log(value.prio);
	var prio='<img src="/img/prio0.png">';
	break;
	case 1:
	var prio='<img src="/img/prio1.png">';
	break;
	case 2:
	var prio='<img src="/img/prio2.png">';
	break;
	case 3:
	var prio='<img src="/img/prio3.png">';
	break;
}
                    $('#wiedervolage_response').append(
                        '<tr>'+
                        '<th>'+prio+'</th>'+
                        '<th> '+value.id+'</th>'+
                        '<th> '+value.date+'</th>'+
                        '<th> '+value.vorname+' '+value.nachname+'</th>'+
                        '<th> '+value.kreditbetrag+'</th>'+
                        '<th> '+value.status_intern+'</th>'+
                        '<th> '+value.wiedervorlage_user+'</th>'+
                        '<th> '+value.last_comment.inhalt+'</th>'+
                        '<th> <a class="btn btn-primary" target="_blank" href="/<?php echo $admin_type?>/credit/edit/'+value.id+'"><i class="fa fa-pencil"></i> </a></tr>'
                    );

                });









            }
        });

    })

</script>

</body>

</html>