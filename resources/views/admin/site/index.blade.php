@extends('admin.layouts.app')


@section('css')

@endsection


@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false"
                 data-widget-deletebutton="false" data-widget-colorbutton="false">

                {{--
                <header>
                    <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                    <h2>{{trans('personal_credits.index_table_title')}}</h2>

                </header>
                --}}

                <!-- widget div-->
                <div>

                    <!-- widget content -->
                    <div class="widget-body no-padding">

                        <table id="dt_basic" class="table table-striped table-bordered table-hover"
                               width="100%">
                            <thead>
                            <tr>
                                <th data-hide="phone">#</th>
                                <th class="no-sort">
                                    <i class="fa fa-fw fa-user text-muted hidden-md hidden-sm hidden-xs"></i>
                                    URL
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($sites as $site)
                                    <tr>
                                        <td></td>
                                        <td>
                                            <a href="{{$site->url}}">{{$site->url}}</a>
                                        </td>
                                    </tr>
                                @endforeach
                            
                            </tbody>
                        </table>

                    </div>
                    <!-- end widget content -->

                </div>
                <!-- end widget div -->

            </div>
            <!-- end widget -->

        </article>
        <!-- WIDGET END -->

    </div>

    <!-- end row -->


</section>
<!-- end widget grid -->

@endsection



@section('scripts')

<!-- PAGE RELATED PLUGIN(S) -->
<script src="<?=asset('js/admin/plugin/datatables/jquery.dataTables.min.js')?>"></script>
<script src="<?=asset('js/admin/plugin/datatables/dataTables.bootstrap.min.js')?>"></script>
<script src="<?=asset('js/admin/plugin/datatable-responsive/datatables.responsive.min.js')?>"></script>
<script src="<?=asset('js/admin/plugin/datatables/dataTables.fnPagingInfo.js')?>"></script>

<script>

    $(document).ready(function () {

        /* BASIC ;*/
        var responsiveHelper_dt_basic = undefined;
        var responsiveHelper_datatable_fixed_column = undefined;
        var responsiveHelper_datatable_col_reorder = undefined;
        var responsiveHelper_datatable_tabletools = undefined;

        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };

        var dTable = $('#dt_basic').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                "t" +
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
            "autoWidth": true,
            "ordering":false,
            "aoColumnDefs" : [ {
                "bSortable" : false,
                "aTargets" : [ "no-sort" ]
            } ],
            "columnDefs": [{
                "searchable": false,
                "orderable": false,
                "targets": 0
            }],
            "preDrawCallback": function () {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_dt_basic) {
                    responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                }
            },
            "rowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                var page = this.fnPagingInfo().iPage;
                var length = this.fnPagingInfo().iLength;
                var index = (page * length + (iDisplayIndex +1));
                $('td:eq(0)', nRow).html(index);
            },
            "drawCallback": function (oSettings) {
                responsiveHelper_dt_basic.respond();
            }
        });


    });

</script>

@endsection