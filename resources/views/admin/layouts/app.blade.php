<?php
$icons = array(
    'dashboard_index' => 'fa-home',
    'site_index' => 'fa-files-o',
    'visitor_index' => 'fa-edit',
    'visit_index' => 'fa-edit',
    'mailchimp_index' => 'fa-edit',
);


$ribbon = [
    'dashboard' => ['/admin/dashboard', 'Dashboard'],
    'site' => ['/admin/sites', 'Sites'],
    'visitor' => ['/admin/visitors', 'Visitors'],
    'visit' => ['/admin/visits', 'Visits'],
    'mailchimp' => ['/admin/mailchimp', 'Mailchimp'],
];

?>

        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

    <title>Atom</title>
    <meta name="description" content="">
    <meta name="author" content="Atom">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?=asset('css/admin/bootstrap.min.css')?>">
    <link rel="stylesheet" type="text/css" media="screen" href="<?=asset('css/admin/font-awesome.min.css')?>">

    <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
    <link rel="stylesheet" type="text/css" media="screen"
          href="<?=asset('css/admin/smartadmin-production-plugins.min.css')?>">
    <link rel="stylesheet" type="text/css" media="screen" href="<?=asset('css/admin/smartadmin-production.min.css')?>">
    <link rel="stylesheet" type="text/css" media="screen" href="<?=asset('css/admin/smartadmin-skins.min.css')?>">

    <!-- SmartAdmin RTL Support  -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?=asset('css/admin/smartadmin-rtl.min.css')?>">

<!-- We recommend you use "your_style.css" to override SmartAdmin
         specific styles this will also ensure you retrain your customization with each SmartAdmin update.
    <link rel="stylesheet" type="text/css" media="screen" href="<?=asset('css/admin/your_style.css')?>"> -->

    <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?=asset('css/admin/demo.min.css')?>">

@yield('css')



<!-- FAVICONS -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('img/favicon/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('img/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('img/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('img/favicon/site.webmanifest')}}">
    <link rel="mask-icon" href="{{asset('img/favicon/safari-pinned-tab.svg')}}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <!-- GOOGLE FONT -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

    <!-- Specifying a Webpage Icon for Web Clip
         Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
    <link rel="apple-touch-icon" href="img/splash/sptouch-icon-iphone.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/splash/touch-icon-ipad.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/splash/touch-icon-iphone-retina.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/splash/touch-icon-ipad-retina.png">

</head>
<body>

<!-- HEADER -->
<header id="header">
    <div id="logo-group">

        <!-- PLACE YOUR LOGO HERE -->
        <span id="logo"> <img src="<?=asset('images/admin/logo.png')?>" alt="BR OfficeG"> </span>
        <!-- END LOGO PLACEHOLDER -->

    </div>

    <!-- pulled right: nav area -->
    <div class="pull-right">

        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
            <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i
                            class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->

        <!-- #MOBILE -->
        <!-- Top menu profile link : this shows only when top menu is active -->
        <ul id="mobile-profile-img" class="header-dropdown-list hidden-xs padding-5">
            <li class="">
                <a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown">
                    <img src="<?=asset('images/admin/avatars/sunny.png')?>" alt="" class="online"/>
                </a>
                <ul class="dropdown-menu pull-right">
                    <li>
                        <a href="profile.html" class="padding-10 padding-top-0 padding-bottom-0"> <i
                                    class="fa fa-user"></i> <u>P</u>rofile</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0"
                           data-action="launchFullscreen"><i class="fa fa-arrows-alt"></i> Full <u>S</u>creen</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="/logout" class="padding-10 padding-top-5 padding-bottom-5" data-action="userLogout"><i
                                    class="fa fa-sign-out fa-lg"></i> <strong><u>L</u>ogout</strong></a>
                    </li>
                </ul>
            </li>
        </ul>

        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
            <span> <a href="/logout" title="Sign Out" data-action="userLogout"
                      data-logout-msg="You can improve your security further after logging out by closing this opened browser"><i
                            class="fa fa-sign-out"></i></a> </span>
        </div>
        <!-- end logout button -->

        <!-- fullscreen button -->
        <div id="fullscreen" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i
                            class="fa fa-arrows-alt"></i></a> </span>
        </div>
        <!-- end fullscreen button -->

    </div>
    <!-- end pulled right: nav area -->

</header>
<!-- END HEADER -->

@include('admin.layouts.nav')

<!-- MAIN PANEL -->
<div id="main" role="main">

    <!-- RIBBON -->
    <div id="ribbon">

        <!-- breadcrumb -->
        <ol class="breadcrumb">
            @if ($controller_name == 'home')
                <li>Home</li>
            @else
                <li><a href="/admin">Home</a></li>
                @if ($action_name == 'index')
                    <li>{{$ribbon[$controller_name][1]}}</li>
                @else
                    <li><a href="{{$ribbon[$controller_name][0]}}">{{$ribbon[$controller_name][1]}}</a></li>
                    <li style="text-transform: capitalize;">{{$action_name}}</li>
                @endif

            @endif


        </ol>
        <!-- end breadcrumb -->

        <!-- You can also add more buttons to the
        ribbon for further usability

        Example below:

        <span class="ribbon-button-alignment pull-right">
        <span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
        <span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
        <span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
        </span> -->

    </div>
    <!-- END RIBBON -->

    <!-- MAIN CONTENT -->
    <div id="content">


        <!-- col -->
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark"><!-- PAGE HEADER --><i
                        class="fa-fw fa {{$icons[$controller_name . '_' . $action_name]}}"></i> {{$controller_name}}
            </h1>
        </div>
        <!-- end col -->

        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8 text-right">

        </div>

        @yield('content')
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->


<!-- PAGE FOOTER -->
<div class="page-footer">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <span class="txt-color-white">Atom <span class="hidden-xs"> - &copy; </span><?php echo date('Y'); ?></span>
        </div>

        <div class="col-xs-6 col-sm-6 text-right hidden-xs">
            <div class="txt-color-white inline-publication">

            </div>
        </div>
    </div>
</div>
<!-- END PAGE FOOTER -->

<!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
Note: These tiles are completely responsive,
you can add as many as you like
-->
<div id="shortcut">
    <ul>
        <li>
            <a href="/personal/admin-profile" class="jarvismetro-tile big-cubes selected bg-color-pinkDark"> <span
                        class="iconbox"> <i
                            class="fa fa-user fa-4x"></i> <span>{{trans('personal_nav.admin_profile')}}</span> </span>
            </a>
        </li>
    </ul>
</div>
<!-- END SHORTCUT AREA -->

<!--================================================== -->

<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }'
        src="<?=asset('js/admin/plugin/pace/pace.min.js')?>"></script>

<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    if (!window.jQuery) {
        document.write('<script src="<?=asset('js/admin/libs/jquery-3.2.1.min.js')?>"><\/script>');
    }
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script>
    if (!window.jQuery.ui) {
        document.write('<script src="<?=asset('js/admin/libs/jquery-ui.min.js')?>"><\/script>');
    }
</script>

<!-- IMPORTANT: APP CONFIG -->
<script src="<?=asset('js/admin/app.config.js')?>"></script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="<?=asset('js/admin/plugin/jquery-touch/jquery.ui.touch-punch.min.js')?>"></script>

<!-- BOOTSTRAP JS -->
<script src="<?=asset('js/admin/bootstrap/bootstrap.min.js')?>"></script>

<!-- CUSTOM NOTIFICATION -->
<script src="<?=asset('js/admin/notification/SmartNotification.min.js')?>"></script>

<!-- JARVIS WIDGETS -->
<script src="<?=asset('js/admin/smartwidgets/jarvis.widget.min.js')?>"></script>

<!-- SPARKLINES -->
<script src="<?=asset('js/admin/plugin/sparkline/jquery.sparkline.min.js')?>"></script>

<!-- JQUERY VALIDATE -->
<script src="<?=asset('js/admin/plugin/jquery-validate/jquery.validate.min.js')?>"></script>

<!-- JQUERY MASKED INPUT -->
<script src="<?=asset('js/admin/plugin/masked-input/jquery.maskedinput.min.js')?>"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="<?=asset('js/admin/plugin/select2/select2.min.js')?>"></script>

<!-- JQUERY UI + Bootstrap Slider -->
<script src="<?=asset('js/admin/plugin/bootstrap-slider/bootstrap-slider.min.js')?>"></script>

<!-- browser msie issue fix -->
<script src="<?=asset('js/admin/plugin/msie-fix/jquery.mb.browser.min.js')?>"></script>

<!-- FastClick: For mobile devices -->
<script src="<?=asset('js/admin/plugin/fastclick/fastclick.min.js')?>"></script>

<!--[if IE 8]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

<![endif]-->

<!-- MAIN APP JS FILE -->
<script src="<?=asset('js/admin/app.min.js')?>"></script>


@yield('js')

<script>
    $(document).ready(function () {
        // DO NOT REMOVE : GLOBAL FUNCTIONS!
        pageSetUp();
    });
</script>

@yield('scripts')

</body>
</html>
