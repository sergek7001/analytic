


<!-- Left panel : Navigation area -->
<!-- Note: This width of the aside area can be adjusted through LESS variables -->
<aside id="left-panel">

    <!-- User info -->
    <div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as it -->

					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
						<img src="<?=asset('images/admin/avatars/sunny.png')?>" alt="me" class="online"/>
						<span>

						</span>
						<i class="fa fa-angle-down"></i>
					</a> 
					
				</span>
    </div>
    <!-- end user info -->

    <!-- NAVIGATION : This navigation is also responsive-->
    <nav>

        <ul>

            <li class="{{ $controller_name=='home'?'active':'inactive' }}">
                <a href="/admin/dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span
                            class="menu-item-parent">Dashboard</span></a>
            </li>

            <li class="{{ $controller_name=='products'?'active':'inactive' }}">
                <a href="/admin/sites"><i class="fa fa-lg fa-fw fa-pied-piper"></i>
                    <span class="menu-item-parent">Sites</span>
                </a>
            </li>

            <li class="{{ $controller_name=='categories'?'active':'inactive' }}">
                <a href="/admin/visitors"><i class="fa fa-lg fa-fw fa-group"></i>
                    <span class="menu-item-parent">Visitors</span>
                </a>
            </li>

            <li class="{{ $controller_name=='persons'?'active':'inactive' }}">
                <a href="/admin/visits"><i class="fa fa-lg fa-fw fa-user"></i>
                    <span class="menu-item-parent">Visits</span>
                </a>
            </li>

            <li class="{{ $controller_name=='slides'?'active':'inactive' }}">
                <a href="/admin/mailchimp"><i class="fa fa-lg fa-fw fa-file-image-o"></i>
                    <span class="menu-item-parent">Mailchimp</span>
                </a>
            </li>

        </ul>

    </nav>


    <span class="minifyme" data-action="minifyMenu">
				<i class="fa fa-arrow-circle-left hit"></i> 
			</span>

</aside>
<!-- END NAVIGATION -->