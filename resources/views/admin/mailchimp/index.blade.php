@extends('admin.layouts.app')


@section('css')
    <style>
        .label{
            color:#3a3633!important;
            font-size: 100%!important;
        }

        input{
            margin-bottom: 20px;
        }
    </style>
@endsection


@section('content')

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <!-- NEW WIDGET START -->
            <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <label class="label" for="api_key"> Mailchimp API Key</label>
                <input class="form-control" type="text" id="api_key" placeholder="api_key" required="required">
                <label class="label" for="api_key"> Mailchimp List ID</label>
                <input class="form-control" type="text" id="list_id" placeholder="list_id" required="required">
                <label class="label" for="api_key"> Email for push to list</label>
                <input class="form-control" type="email" id="email" placeholder="email" required="required">
                <input class="btn btn-primary" type="button" value="send" onclick="pushUser()">

                <div id="result">

                </div>


            </article>
            <!-- WIDGET END -->

        </div>

        <!-- end row -->

    </section>
    <!-- end widget grid -->

@endsection



@section('scripts')

    <script>

        function pushUser() {
            $('#result').html('Sending...');

            $.ajax({
                url: "/admin/mailchimp/push-user",
                type: "POST",
                dataType: "JSON",
                data: {
                    _token: '{{csrf_token()}}',
                    api_key: $('#api_key').val(),
                    list_id: $('#list_id').val(),
                    email: $('#email').val(),
                },
                success: function (response) { //Данные отправлены успешно
                    $('#result').html(response)
                },
                error: function (response) { // Данные не отправлены
                    //console.log(response);
                    $('#result').html('Error');
                }
            });
        }

        $(document).ready(function () {

        });

    </script>

@endsection