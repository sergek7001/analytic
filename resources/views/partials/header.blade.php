<header class="page-header">
    <div class="page-header-greenbar">
        <div class="container">
            <div class="flex">
        	<span class="hidden-xs">
        		<img src="/img/icons/icon_pfeil.png" alt="" title="">
        		So erreichen Sie uns <strong>direkt</strong>:
        	</span>
                <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span><strong>030-60985721</strong>
                <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>&#105;&#110;&#102;&#111;&#064;&#099;&#114;&#101;&#100;&#105;&#099;&#111;&#109;&#046;&#100;&#101;
            </div>
        </div>
    </div>
    <div class="page-header-whitebar">
        <nav class="navbar navbar-default">
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#navbar" aria-expanded="false"><span class="sr-only">Navigation ein-/ausklappen</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                    <div class="navbar-brand">
                        <a class="navbar-brand-image" href="/" title=""><img src="img/logo_header.png" alt="" title=""></a>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navbar">
                    <ul class="nav navbar-nav navbar-right">
                    <!--<li@if($menu==1){echo ' class="active"';};?>><a href="/" title=""><span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>-->

                        <li>
                            <button class="btn btn-orange navbar-btn text-upper" type="button" onclick="window.location.href='/kreditanfrage'">Anfrage<span class="glyphicon glyphicon-menu-right"></span></button>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>