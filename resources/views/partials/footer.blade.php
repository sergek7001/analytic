<footer style="position:relative;" class="page-footer" >
    <section class="page-footer-widget widget-container" id="widget-last">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <img src="img/logo_footer.png" alt="" title="" class="pt-lg pb-lg" />
                    <div class="row">
                        <div class="col-xs-2"><span class="glyphicon glyphicon-map-marker glyphicon-green"></span></div>
                        <div class="col-xs-10">
                            <p>
                                Credicom GmbH<br />
                                Uhlandstraße 20-25<br />
                                10623 Berlin
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <p class="pt-md mb-xs text-sm">Rufen Sie uns an:</p>
                    <p class="mb-xs text-xxl text-green font-weight-bold font-kursiv"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span> 030-60985721</p>
                    <p class="text-sm">oder senden Sie uns ein Fax: 030-60985722</p>
                    <p class="pt-lg mb-xs text-sm">E-Mails an:</p>
                    <p class="text-lg text-green font-weight-bold font-kursiv"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> &#105;&#110;&#102;&#111;&#064;&#099;&#114;&#101;&#100;&#105;&#099;&#111;&#109;&#046;&#100;&#101;</p>
                </div>
                <div class="clearfix hidden-lg hidden-md"></div>
                <div class="col-md-1 col-sm-6">
                    <ul>
                        <li><a href="kontakt" title="">Kontakt</a></li>
                        <li><a href="faq" title="">FAQ</a></li>
                        <li><a href="ueber-uns" title="">&Uuml;ber uns</a></li>
                        <li><a href="impressum" title="">Impressum</a></li>
                        <li><a href="agb" title="">AGB</a></li>
                        <li><a href="datenschutz" title="">Datenschutz</a></li>
                    </ul>
                </div>
                <div class="col-md-5 col-sm-6">
                    <div class="row pt-lg">
                        <div class="col-xs-12 col-sm-4">
                            <a href="https://www.ekomi.de/bewertungen-credicomde.html" title="" target="_blank"><img src="img/footer_ekomi.png" alt="" title=""></a>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <a href="https://verbraucherschutz.de/credicom-kredite-berlin/" title="" target="_blank"><img src="img/footer_vschutz.png" alt="" title=""></a>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <img src="img/footer_bcheck.png" alt="" title="">
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr class="weiss mt-xs mb-xl" />
                <a href="sofortkredit" title="Sofortkredit" class="btn btn-orange">Sofortkredit</a>
                <a href="kredit-ohne-schufa" title="Kredit ohne Schufa" class="btn btn-orange">Kredit ohne Schufa</a>
                <a href="umschuldung" title="Umschuldung" class="btn btn-orange">Umschuldung</a>
                <a href="kredit-online-beantragen" title="Onlinekredit" class="btn btn-orange">Onlinekredit</a>
                <a href="ratenkredit" title="Ratenkredit" class="btn btn-orange">Ratenkredit</a>
                <a href="urlaubskredit" title="Urlaubskredit" class="btn btn-orange">Urlaubskredit</a>
                <a href="kredit-fuer-selbststaendige" title="Kredit f&uuml;r Selbstst&auml;ndige" class="btn btn-orange">Kredit f&uuml;r Selbstst&auml;ndige</a>
                <a href="autokredit" title="Autokredit" class="btn btn-orange">Autokredit</a>
                <a href="baufinanzierung" title="Baufinanzierung" class="btn btn-orange">Baufinanzierung</a>
            </div>
        </div>
    </section>
</footer>


<div class="container">
    <div class="row">
        <div class="col-md-12">
            <p class="aligncenter text-sm">
                Angaben gem. &sect; 6a PAngV: Unver&auml;nderlicher Sollzinssatz ab 3,44%, effektiver Jahreszins ab 3,49% - 15,95%, Nettodarlehensbetrag ab 1000,- bis 300.000,- &euro;, Laufzeit von 12 bis 120 Monaten, Bonit&auml;t vorausgesetzt. Repr&auml;sentatives Beispiel: Sollzinssatz 5,92% fest f&uuml;r die gesamte Laufzeit, Effektiver Jahreszins: 6,09%, Nettokreditbetrag:  10.000,- &euro;, Vertragslaufzeit: 60 Monate, Monatliche Rate: 193,00 &euro;, Gesamter Zinsaufwand: 1573,98 &euro;, Gesamtr&uuml;ckzahlung (inkl. aller Geb&uuml;hren): 11.576,98 &euro;.
            </p>
        </div>
    </div>
</div>