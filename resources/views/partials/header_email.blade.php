<header id="header">
    <div id="logo-group">

        <!-- PLACE YOUR LOGO HERE -->
        <span id="logo"> <img src="/smartAdmin/img/logo.png" alt="SmartAdmin"> </span>
        <!-- END LOGO PLACEHOLDER -->



    </div>



    <!-- pulled right: nav area -->
    <div class="pull-right">


    @php
        if(Auth::guard('admin')->user()!==null){
        $logout='/admin/logout';
        }
        else if(Auth::guard('superadmin')->user()!==null){
        $logout='/superadmin/logout';
        }
        else{
        $logout='/#';
        }

    @endphp



        <!-- fullscreen button -->
        <div id="fullscreen" class="btn-header transparent pull-right">
            <span> <a href="javascript:void(0);" data-action="launchFullscreen" title="Full Screen"><i class="fa fa-arrows-alt"></i></a> </span>
        </div>
        <!-- end fullscreen button -->





    </div>
    <!-- end pulled right: nav area -->

</header>