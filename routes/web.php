<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('visit/store', 'Visit\StoreController@store');
Route::get('visit/store', 'Visit\StoreController@store');

Route::group(['prefix' => 'admin'], function () {
    Route::get('/dashboard', 'Visit\DashboardController@index');
    Route::get('/sites', 'Visit\SiteController@index');
    Route::get('/visitors', 'Visit\VisitorController@index');
    Route::get('/visits', 'Visit\VisitController@index');

    //Mailchimp
    Route::get('/mailchimp', 'MailchimpController@index');
    Route::post('/mailchimp/push-user', 'MailchimpController@pushToMailchimp');
});
