let startTime = performance.now();

let analyticVisit = function() {
    // Check to have support
    if (!navigator.sendBeacon) return true;

    // URL to send the data
    let url = '';

    // Data to store
    let data = new FormData();
    data.append('start', startTime);
    data.append('end', performance.now());
    data.append('url', document.URL);
    data.append('host', window.location.hostname);
    data.append('user_agent', navigator.userAgent);

    navigator.sendBeacon(url, data);
};

window.onunload = function analytics(event) {
    if (!navigator.sendBeacon) return;
    analyticVisit();
};